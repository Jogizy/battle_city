note
	description: "Le moteur principal du jeu. L'image est rafra�chie � chaque it�ration"
	auteur: "N'zu� Kouadio Ange Kouassi & Olivier M"
	date: "2020-05-16"
	revision: "5.1"

deferred class
	GAME_ENGINE

inherit
	ENGINE_TEXT
		rename
			make as make_engine
		end
	GAME_LIBRARY_SHARED
	AUDIO_LIBRARY_SHARED
	GAME_RANDOM_SHARED

feature {NONE} -- Initialisation

	make(a_window:GAME_WINDOW_RENDERED)
			-- Initialisation de `Current'
		do
			level := 1
			score := 0
			make_engine(a_window)
			create audio_source_factory.make
			initialize_class
			initialize_font
			initialize_all_lists
			initialize_enemy_spawn
			initialize_text
		end

	initialize_class
			-- Initialisation des classes
		do
			create tile_factory.make(audio_source_factory)
			create map_management.make(tile_factory)
			map := map_management.current_map
			create hero.make(audio_source_factory)
			create fullscreen.make(window)
			create music.make(audio_source_factory)
			create enemy_factory.make(audio_source_factory)
			create enemy_management.make(enemy_factory)
		end

	initialize_all_lists
			-- Initialisation de toutes les listes
		do
			create {ARRAYED_LIST[PROJECTILE]}projectiles.make(1)
			create {ARRAYED_LIST[EXPLOSION]}explosions.make(1)
			create {ARRAYED_LIST[TUPLE[x,y:INTEGER]]}enemy_spawn_coordinates.make(3)
			create {ARRAYED_LIST[COLLISABLE]}collisable_list_object.make(1)
			initialize_collisable_list_object
		end

	initialize_enemy_spawn
			-- Initialise l'apparition des ennemis
		do
			enemy_spawn_coordinates.wipe_out
			enemy_spawn_coordinates.extend([0,0])
			enemy_spawn_coordinates.extend([96,0])
			enemy_spawn_coordinates.extend([192,0])
		end

	initialize_enemy_management
			-- Initialise les ennemis � leur cr�ation
		do
			enemy_management.enemies.wipe_out
			enemy_management.enemies.extend(enemy_factory.new_char(0,0,game_library.time_since_create))
			enemy_management.enemies.extend(enemy_factory.new_panzer(96,0,game_library.time_since_create))
			enemy_management.enemies.extend(enemy_factory.new_tank(192,0,game_library.time_since_create))
		end

	initialize_font
			-- Initialise la police texte
		do
			font := load_font("font.ttf",9)
		end

	initialize_text
			-- Initialise le text de `score'
		do
			texts.wipe_out
			texts.extend([1, 3,"Score: " + score.out , create {GAME_COLOR}.make_rgb(0,255,255),font])
		end

feature -- Acc�s

	run
			-- Lance le jeu	
		do
			music.play
			reset_game

			game_library.quit_signal_actions.extend(agent on_quit)
			window.key_pressed_actions.extend(agent on_key_pressed)
			window.key_released_actions.extend(agent on_key_released)
			game_library.iteration_actions.extend(agent on_iteration)
			if window.renderer.driver.is_present_synchronized_supported then
				game_library.launch_no_delay
			else
				game_library.launch
			end
			game_library.clear_all_events
		end

	set_level(a_level:INTEGER)
			-- Attribue la valeur de 'a_level' � 'level'
		do
			level := a_level
		end

	-- D�claration des attributs

	music:MUSIC
			-- La musique principal

	audio_source_factory:AUDIO_SOURCE_FACTORY
			-- Le g�n�rateur d'audio

	tile_factory:TILE_FACTORY
			-- Le g�n�rateur de tuiles

	enemy_factory:ENEMY_FACTORY
			-- Le g�n�rateur d'ennemi

	hero:HERO
			-- Le personnage principal du jeu

	map:MAP
			-- La carte du jeu

	map_management:MAP_MANAGEMENT
			-- La gestion des diff�rentes cartes du jeu

	enemy_management:ENEMY_MANAGEMENT
			-- La gestion des diff�rents ennemis

	fullscreen:FULLSCREEN
			-- Le mode d'affichage de l'�cran

	projectiles:LIST[PROJECTILE]
			-- La liste de projectiles

	explosions:LIST[EXPLOSION]
			-- La liste d'explosions

	collisable_list_object:LIST[COLLISABLE]
			-- La liste d'objets collisables

	enemy_spawn_coordinates:LIST[TUPLE[x,y:INTEGER]]
			-- La liste de coordonn�es des ennemis

	font:TEXT_FONT
			-- La police utilis� pour dessiner du texte

	level:INTEGER
			-- Le niveau du joueur

	number_of_enemies_killed:NATURAL
			-- Le nombre d'ennemis d�truit

	game_over:BOOLEAN
			-- Vrai si la partie est termin�, sinon faux

	you_win:BOOLEAN
			-- Vrai si la partie est gagn�e, sinon faux

	is_menu:BOOLEAN
		-- vrai si c'est le menu, sinon faux

	is_next_level:BOOLEAN
		-- Vrai si c'est le niveau pr�c�dent est termin�, sinon faux

	score:INTEGER
		-- Le score de la partie

feature {NONE} -- Impl�mentation

	on_iteration(a_timestamp:NATURAL_32)
			-- �v�nement qui est lanc� � chaque it�ration.
		do
			audio_library.update
			-- Redessine la sc�ne
			window.renderer.set_drawing_color(create {GAME_COLOR}.make_rgb(16, 16, 16)) -- Couleur de l'arri�re plan
			window.renderer.draw_filled_rectangle(0, 0, texture.width, texture.height)

			update_explosions
			update_projectiles(a_timestamp)

			draw_displayables(explosions)
			draw_displayables(projectiles)
			draw_displayable(hero)
			draw_displayables(enemy_management.enemies)
			draw_displayables(map.map_coordinates)
			draw_texts

			block_objects_outbound_from_screen(a_timestamp)
			collision_unit(hero)
			action_enemy(a_timestamp)

			-- Met � jour la modification � l'�cran
			window.renderer.present
		end

	block_objects_outbound_from_screen(a_timestamp:NATURAL_32)
			-- Assure que les objets ne sortent pas de l'�cran
		do
			block_hero_outbound_from_screen(a_timestamp)
			block_enemies_outbound_from_screen(a_timestamp)
			block_projectiles_outbound_from_screen(a_timestamp)
		end

	on_key_pressed(a_timestamp:NATURAL_32; a_key_event:GAME_KEY_EVENT)
			-- Action lorsqu'une touche du clavier a �t� enfonc�e
		do
			if not a_key_event.is_repeat then
				hero_control(a_timestamp, a_key_event)
				hero.is_moving
				window_control(a_timestamp, a_key_event)
				volume_control(a_key_event)
			end
		end

	hero_control(a_timestamp:NATURAL_32; a_key_event:GAME_KEY_EVENT)
			-- Contr�le le mouvement de l'h�ro et son tir
		do
			if a_key_event.is_right then
				hero.go_right(a_timestamp)
			elseif a_key_event.is_left then
				hero.go_left(a_timestamp)
			elseif a_key_event.is_up then
				hero.go_up(a_timestamp)
			elseif a_key_event.is_down then
				hero.go_down(a_timestamp)
			end
		end

	volume_control(a_key_event:GAME_KEY_EVENT)
			-- Contr�le le volume de la musique du jeu et des autres sons
		do
			if a_key_event.is_m then
				music.toggle_music
			elseif a_key_event.is_equals then
				audio_source_factory.add_gain(0.1)
			elseif a_key_event.is_minus then
				audio_source_factory.add_gain(-0.1)
			end
		end

	window_control(a_timestamp:NATURAL_32; a_key_event:GAME_KEY_EVENT)
			-- Contr�le la fen�tre du jeu
		do
			if a_key_event.is_escape then
				on_quit(a_timestamp)
			elseif a_key_event.is_f11 then
				fullscreen.toggle_fullscreen
			end
		end


	on_key_released(a_timestamp:NATURAL_32; a_key_event:GAME_KEY_EVENT)
			-- Action lorsqu'une touche du clavier a �t� rel�ch�e
		do
			if not a_key_event.is_repeat then
				if not game_over then
					if a_key_event.is_right then
						hero.stop_right
					elseif a_key_event.is_left then
						hero.stop_left
					elseif a_key_event.is_up then
						hero.stop_up
					elseif a_key_event.is_down then
						hero.stop_down
					end
				end
				hero.is_moving
			end
		end

	on_quit(a_timestamp: NATURAL_32)
			-- Cette m�thode est appel�e lorsque le signal de sortie est envoy�
			-- � l'application (ex: bouton X de la fen�tre).
		do
			-- Arr�te la boucle du contr�leur (autorise game_library.launch � revenir)
			game_library.stop
		end

feature -- Acc�s

	initialize_collisable_list_object
			-- Initialisation de la liste d'objets collisables
		do
			collisable_list_object.wipe_out
			collisable_list_object.append(enemy_management.enemies)
			collisable_list_object.extend(hero)
			from
				map.map_coordinates.start
			until
				map.map_coordinates.exhausted
			loop
				if attached{COLLISABLE}map.map_coordinates.item as la_collisable then
					collisable_list_object.extend(la_collisable)
				end
				map.map_coordinates.forth
			end
		end

	block_hero_outbound_from_screen(a_timestamp:NATURAL_32)
			-- Cette m�thode emp�che l'h�ro de sortir de la zone de jeu
		do
			hero.update(a_timestamp) -- Met � jour l'h�ro
			if hero.x < 0 then
				hero.x := 0
			elseif hero.x + hero.sub_image_width > window.renderer.logical_size.width then
				hero.x := window.renderer.logical_size.width - hero.sub_image_width
			elseif hero.y < 0 then
				hero.y := 0
			elseif hero.y + hero.sub_image_height > window.renderer.logical_size.height then
				hero.y := window.renderer.logical_size.height - hero.sub_image_height
			end
		end

	block_projectiles_outbound_from_screen(a_timestamp:NATURAL_32)
			-- Cette m�thode emp�che les projectiles de sortir de la zone de jeu
		local
			l_projectile:PROJECTILE
		do
			from
				projectiles.start
			until
				projectiles.exhausted
			loop
				l_projectile := projectiles.item
				l_projectile.update(a_timestamp) -- Met � jour le projectile
				if l_projectile.x < 0 then
					remove_projectile
				elseif l_projectile.x + l_projectile.sub_image_width > window.renderer.logical_size.width then
					remove_projectile
				elseif l_projectile.y < 0 then
					remove_projectile
				elseif l_projectile.y + l_projectile.sub_image_height > window.renderer.logical_size.height then
					remove_projectile
				else
					projectiles.forth
				end
			end
		end

	block_enemies_outbound_from_screen(a_timestamp:NATURAL_32)
			-- Cette m�thode emp�che les ennemis de sortir de la zone de jeu
		local
			l_enemy:ENEMY
		do
			across enemy_management.enemies as la_enemies loop
				l_enemy := la_enemies.item
				l_enemy.update(a_timestamp) -- Met � jour l'ennemi
				if l_enemy.x < 0 then
					l_enemy.x := 0
					l_enemy.has_collided := True
				elseif l_enemy.x + l_enemy.sub_image_width > window.renderer.logical_size.width then
					l_enemy.x := window.renderer.logical_size.width - l_enemy.sub_image_width
					l_enemy.has_collided := True
				elseif l_enemy.y < 0 then
					l_enemy.y := 0
					l_enemy.has_collided := True
				elseif l_enemy.y + l_enemy.sub_image_height > window.renderer.logical_size.height then
					l_enemy.y := window.renderer.logical_size.height - l_enemy.sub_image_height
					l_enemy.has_collided := True
				end
			end
		end

	collision_unit(a_unit:UNIT)
			-- Collision d'une unit� `a_unit' avec les objets collisables
		local
			l_collisable:COLLISABLE
		do
			from
				collisable_list_object.start
			until
				collisable_list_object.exhausted
			loop
				l_collisable := collisable_list_object.item
				if not ((l_collisable.x = a_unit.x) and (l_collisable.y = a_unit.y)) then
					if a_unit.has_collision(l_collisable) then
						if attached{ENEMY}a_unit as la_enemy then
							la_enemy.has_collided := True
						end
						stop_moving(a_unit)
					end
				end
				collisable_list_object.forth
			end
		end

	stop_moving(a_unit:UNIT)
			-- Arr�te le d�placement
		local
			l_x, l_y:INTEGER
		do
			l_x := a_unit.x
			l_y := a_unit.y
			if a_unit.going_right then
				a_unit.x := a_unit.x - a_unit.moving
			elseif a_unit.going_left then
				a_unit.x:= a_unit.x + a_unit.moving
			elseif a_unit.going_up then
				a_unit.y := a_unit.y + a_unit.moving
			elseif a_unit.going_down then
				a_unit.y := a_unit.y - a_unit.moving
			end
			if l_x < 0 then
				l_x := 0
			elseif l_y < 0 then
				l_y := 0
			end
		end

	collision_projectile(a_projectile:PROJECTILE; a_timestamp:NATURAL_32):BOOLEAN
			-- Collision d'un projectile `a_projectile' avec les objets collisables
			-- Retourne vrai si la collision du projectile n'est pas attach� � son unit�, sinon faux.
		local
			l_result:BOOLEAN
			l_has_destroyed:BOOLEAN
			l_collisable:COLLISABLE
		do
			l_result := False
			l_has_destroyed := False
			from
				collisable_list_object.start
			until
				collisable_list_object.exhausted
			loop
				l_collisable := collisable_list_object.item
				if a_projectile.has_collision(l_collisable)then
					if not (attached{UNIT}l_collisable as la_unit and then la_unit.projectile ~ a_projectile) then
						l_result := True
						l_has_destroyed := brick_hit(l_collisable)
						objects_hit(l_collisable,a_projectile, a_timestamp)
					end
				end
				if not l_has_destroyed then
					collisable_list_object.forth
				else
					collisable_list_object.remove
				end
				l_has_destroyed := False
			end
			Result := l_result
		end

	objects_hit(a_collisable:COLLISABLE;a_projectile:PROJECTILE; a_timestamp:NATURAL_32)
			-- Cette m�thode regroupe les objets collis�s
		do
			steel_hit(a_collisable)
			water_hit(a_collisable)
			if not game_over then
				king_hit(a_collisable)
				hero_hit(a_collisable)
				enemy_hit(a_collisable, a_projectile, a_timestamp)
			end
		end

	brick_hit(a_collisable:COLLISABLE):BOOLEAN
			-- Change l'�tat de la tuile brique `a_collisable'
			-- Retourne vrai si la brique a �t� d�truite
		local
			l_result:BOOLEAN
		do
			l_result := False
			if attached{TILE_BRICK}a_collisable as la_brick then
				la_brick.play_hit_brick
				if not la_brick.is_broken then
					la_brick.crack_brick
					la_brick.is_broken := True
				else
					map.map_coordinates.prune_all(la_brick)
					l_result := True
				end
			end
			Result := l_result
		end

	king_hit(a_collisable:COLLISABLE)
			-- Change l'�tat de la tuile king `a_collisable'
		do
			if attached{TILE_KING}a_collisable as la_king then
				if not la_king.is_broken then
					la_king.play_hit_king
					la_king.disintegrate_king
					la_king.is_broken := True
					-- Jeu termin�
					set_game_over
				end
			end
		end

	steel_hit(a_collisable:COLLISABLE)
			-- Joue le son du bloc d'acier `a_collisable'
		do
			if attached{TILE_STEEL}a_collisable as la_steel then
				la_steel.play_hit_steel
			end
		end

	water_hit(a_collisable:COLLISABLE)
			-- Joue le son du bloc d'eau `a_collisable'
		do
			if attached{TILE_WATER}a_collisable as la_water then
				la_water.play_hit_water
			end
		end

	hero_hit(a_collisable:COLLISABLE)
			-- Change l'�tat du h�ro `a_collisable'
		do
			if attached{HERO}a_collisable as la_hero then
				la_hero.damage
				la_hero.play_explode
				la_hero.new_explosion
				if attached la_hero.explosion as la_explosion then
					explosions.extend(la_explosion)
				end
				reposition_hero
				if la_hero.hitpoints = 0 then
					-- Jeu termin�
					set_game_over
					hero.life_restoration
				end
			end
		end

	enemy_hit(a_collisable:COLLISABLE; a_projectile:PROJECTILE; a_timestamp:NATURAL_32)
			-- Change l'�tat de l'ennemi `a_collisable' en fonction du projectile `a_projectile'
		do
			if attached{ENEMY}a_collisable as la_enemy then
				-- Si le projectile provient de h�ro seulement
				if (a_projectile ~ hero.projectile) then
					la_enemy.damage
					if la_enemy.hitpoints = 0 then
						la_enemy.play_explode
						la_enemy.new_explosion
						if attached la_enemy.explosion as la_explosion then
							explosions.extend(la_explosion)
							score := score + 5
							initialize_text
						end
						enemy_management.enemies.prune_all(la_enemy)
						collisable_list_object.remove
						spawn_new_enemy(a_timestamp)
						if number_of_enemies_killed >= 7 then
							if level < 3 then
								-- Niveau sup�rieur			
								is_next_level := True
							else
								you_win := True
							end
							game_library.stop
						end
					end
				end
			end
		end

	action_enemy(a_timestamp:NATURAL_32)
			-- Action des ennemis
		do
			from
				enemy_management.enemies.start
			until
				enemy_management.enemies.exhausted
			loop
				collision_unit(enemy_management.enemies.item)
				random_action(enemy_management.enemies.item, a_timestamp)
				enemy_management.enemies.item.new_direction(a_timestamp)
				enemy_management.enemies.forth
			end
		end

	update_explosions
			-- Met � jour et enl�ve l'explosion de la liste
		do
			from
				explosions.start
			until
				explosions.exhausted
			loop
				explosions.item.update -- Met � jour l'explosion
				if explosions.item.has_finished then
					explosions.remove
				else
					explosions.forth
				end
			end
		end

	update_projectiles(a_timestamp:NATURAL_32)
			-- Enl�ve le projectile de la liste
		local
			l_projectile:PROJECTILE
		do
			from
				projectiles.start
			until
				projectiles.exhausted
			loop
				l_projectile := projectiles.item
				if not collision_projectile(l_projectile, a_timestamp) then
					projectiles.forth
				else
					remove_projectile
				end
			end
		end

	remove_projectile
			-- Suppression du projectile du jeu
		do
			projectiles.item.destroy_bullet
			projectiles.remove
		ensure
			projectile_remove:projectiles.count = old projectiles.count - 1
		end

	load_map(a_number:INTEGER)
			-- Chargement de la carte du jeu en fonction de l'index entier `a_number'
		require
			valid_number: a_number >= 1 and a_number <= map_management.maps.count
		do
			map_management.change_map(a_number)
			map := map_management.current_map
			initialize_collisable_list_object
		end

	fire(a_unit:UNIT; a_timestamp:NATURAL_32)
			-- Tir du projectile par une unit� `a_unit'
		do
			if not a_unit.is_shooting then
				a_unit.play_fire
				a_unit.new_projectile(a_timestamp)
				if attached a_unit.projectile as la_projectile then
					projectiles.extend(la_projectile)
				end
			end
		end

	reposition_hero
			-- Positionnement du h�ro � son emplacement originel
		do
			hero.x := 64
			hero.y := 192
			hero.reset_moving
		end

	spawn_new_enemy(a_timestamp:NATURAL_32)
			-- Apparition d'un nouvel ennemi
		local
			l_integer:INTEGER
		do
			random.generate_new_random
			l_integer := random.last_random_integer_between(1, 3)
			enemy_management.new_enemy(
				enemy_spawn_coordinates[l_integer].x,
				enemy_spawn_coordinates[l_integer].y,
				a_timestamp
				)
			collisable_list_object.extend(enemy_management.enemies.last)
			number_of_enemies_killed := number_of_enemies_killed + 1
		end

	random_action(a_enemy:ENEMY; a_timestamp:NATURAL_32)
			-- Action al�atoire de l'ennemi `a_enemy' (tir ou d�placement)
		local
			l_integer:INTEGER
		do
			random.generate_new_random
			l_integer := random.last_random_integer_between(0, 50)
			if l_integer = 0 then
				fire(a_enemy, a_timestamp)
			elseif l_integer = 50 then
				a_enemy.has_collided := True
			end
		end

	set_game_over
			-- Termine la partie
		do
			hero.stop_moving
			game_over := True
			game_library.stop
		end

	reset_game
			-- Initialise la partie au tout d�but
		do
			initialize_enemy_management
			game_over := False
			you_win := False
			is_next_level:= False
			is_menu:= False
			number_of_enemies_killed := 0
			load_map(level)
			reposition_hero
		end

invariant
	valid_enemy_spawn_coordinates: enemy_spawn_coordinates.count = 3
	valid_tile_position:
		across
			map.map_coordinates as la_coordinates
		all
			(la_coordinates.item.x >= 0 and la_coordinates.item.x <= window.renderer.logical_size.width - map.sub_image_width) and
			(la_coordinates.item.y >= 0 and la_coordinates.item.y <= window.renderer.logical_size.height - map.sub_image_height)
		end

note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
			N'zu� Kouadio Ange Kouassi & Olivier M
			D�partement des techniques de l'informatique
			C�gep de Drummondville
			960, rue Saint-Georges
			Drummondville, (Qu�bec)
			J2C 6A2
			T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859
			Site web: http://www.cegepdrummond.ca
		]"
end


