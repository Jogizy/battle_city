note
	description: "La classe visionneuse de la sc�ne du jeu."
	auteur: "N'zu� Kouadio Ange Kouassi & Olivier M"
	date: "2020-05-04"
	revision: "1.0"

class
	VIEWER_ENGINE

inherit
	ENGINE
		rename
			make as make_engine
		end
	GAME_LIBRARY_SHARED

create
	make

feature {NONE} -- Initialisation

	make(a_socket: NETWORK_STREAM_SOCKET; a_window:GAME_WINDOW_RENDERED)
			-- Initialisation de `Current' � utiliser avec `a_socket'
		do
			make_engine(a_window)
			create network_customer.make(a_socket)
		end

feature -- Acc�s

	run
			-- Lance la visionneuse du jeu
		do
			if attached network_customer as la_network_customer then
				la_network_customer.launch
				pre_run
				la_network_customer.stop
				la_network_customer.join
			else
				pre_run
			end
		end

	pre_run
			-- "Pr�-lance" la visionneuse du jeu
		do
			game_library.quit_signal_actions.extend(agent on_quit)
			window.key_pressed_actions.extend(agent on_key_pressed)
			game_library.iteration_actions.extend(agent on_iteration)
			game_library.launch
		end

	on_iteration(a_timestamp:NATURAL_32)
			-- �v�nement qui est lanc� � chaque it�ration.
		do
			-- Redessine la sc�ne
			window.renderer.set_drawing_color(create {GAME_COLOR}.make_rgb (16, 16, 16)) -- Couleur de l'arri�re plan
			window.renderer.draw_filled_rectangle(0, 0, texture.width, texture.height)
			if attached network_customer.displays as la_customer_displays then
				draw_displayables(la_customer_displays)
			end

			-- Met � jour la modification � l'�cran
			window.renderer.present
		end



	on_key_pressed(a_timestamp:NATURAL_32; a_key_event:GAME_KEY_EVENT)
			-- Action lorsqu'une touche du clavier a �t� enfonc�e
		do
			if not a_key_event.is_repeat then
				if a_key_event.is_space then
					network_customer.send
				end
			end
		end

	on_quit(a_timestamp: NATURAL_32)
			-- Cette m�thode est appel�e lorsque le signal de sortie est envoy�
			-- � l'application (ex: bouton X de la fen�tre).
		do
			-- Arr�te la boucle du contr�leur (autorise game_library.launch � revenir)
			game_library.stop
		end

feature {NONE}

	network_customer:NETWORK_CUSTOMER_GAME
			-- Le r�seau client de `Current'

invariant
note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
			N'zu� Kouadio Ange Kouassi & Olivier M
			D�partement des techniques de l'informatique
			C�gep de Drummondville
			960, rue Saint-Georges
			Drummondville, (Qu�bec)
			J2C 6A2
			T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859
			Site web: http://www.cegepdrummond.ca
		]"
end
