note
	description: "Cette classe ajoute toutes les sources audios en m�moire."
	auteur: "Olivier Meloche & N'zu� Kouadio Ange Kouassi"
	date: "2020-04-09"
	revision: "2.0"

class
	AUDIO_SOURCE_FACTORY

inherit
	AUDIO_LIBRARY_SHARED

create
	make

feature {NONE} -- Initialisation

	make
			-- Ajout des sources audio dans la librairie
		do
			audio_library.sources_add
			source_move := audio_library.last_source_added
			audio_library.sources_add
			source_fire := audio_library.last_source_added
			audio_library.sources_add
			source_music := audio_library.last_source_added
			audio_library.sources_add
			source_explosion := audio_library.last_source_added
			audio_library.sources_add
			source_hit_unit := audio_library.last_source_added
			audio_library.sources_add
			source_hit_steel := audio_library.last_source_added
			audio_library.sources_add
			source_hit_brick := audio_library.last_source_added
			audio_library.sources_add
			source_hit_king := audio_library.last_source_added
			audio_library.sources_add
			source_hit_water := audio_library.last_source_added
		end

feature -- Acc�s

	add_gain(a_value:REAL_32)
			-- Ajoute du gain au volume de toutes les source audio en fonction de la valeur `a_value'
		require
			valid_value:a_value >= -1 and a_value <= 1
		do
			from
				audio_library.sources.start
			until
				audio_library.sources.exhausted
			loop
				if audio_library.sources.item.gain + a_value <= 1 and audio_library.sources.item.gain + a_value >= 0 then
					audio_library.sources.item.set_gain(audio_library.sources.item.gain + a_value)
				end
				audio_library.sources.forth
			end
		end

	source_move:AUDIO_SOURCE
			-- L'audio de d�placement du {HERO}

	source_fire:AUDIO_SOURCE
			-- L'audio du projectile d'un {UNIT}

	source_music:AUDIO_SOURCE
			-- La musique principale

	source_explosion:AUDIO_SOURCE
			-- L'audio d'un {UNIT} qui explose

	source_hit_unit:AUDIO_SOURCE
			-- L'audio d'un {UNIT} touch�

	source_hit_steel:AUDIO_SOURCE
			-- L'audio d'un {TILE_STEEL} touch�

	source_hit_brick:AUDIO_SOURCE
			-- L'audio d'un {TILE_BRICK} touch�

	source_hit_king:AUDIO_SOURCE
			-- L'audio d'un {TILE_KING} touch�

	source_hit_water:AUDIO_SOURCE
			-- L'audio d'un {TILE_WATER} touch�

invariant
note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
			N'zu� Kouadio Ange Kouassi & Olivier M
			D�partement des techniques de l'informatique
			C�gep de Drummondville
			960, rue Saint-Georges
			Drummondville, (Qu�bec)
			J2C 6A2
			T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859
			Site web: http://www.cegepdrummond.ca
		]"
end
