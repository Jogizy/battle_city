note
	description: "Tests pour la classe {HERO}."
	generateur: "EiffelStudio test wizard"
	auteur: "N'zu� Kouadio Ange Kouassi & Olivier M"
	date: "24-05-2020"
	revision: "0.2"
	testing: "type/manual", "covers/{BANK_ACCOUNT}"

class
	HERO_TEST

inherit
	EQA_TEST_SET
		redefine
			on_prepare,
			on_clean
		end
	GAME_LIBRARY_SHARED
		undefine
			default_create
		end
	IMG_LIBRARY_SHARED
		undefine
			default_create
		end

	AUDIO_LIBRARY_SHARED
		undefine
			default_create
		end

feature {NONE} -- Events

	on_prepare
			-- Cette m�thode est lanc�e avant d'ex�cuter les tests ci-dessous
			-- Permet de g�n�rer des ressources n�cessaires � l'ex�cution des
			-- tests (sans avoir besoin de le faire dans chaque m�thode de test)
		local
			l_window_builder:GAME_WINDOW_RENDERED_BUILDER
		do
			game_library.enable_video
			image_file_library.enable_image (true, false, false)
			audio_library.enable_playback
			create l_window_builder
			l_window_builder.set_dimension(208, 208)
			window := l_window_builder.generate_window
			create audio_source_factory.make
			create hero.make(audio_source_factory)
		end

	on_clean
			-- Cette m�thode est lanc� apr�s l'ex�cution de tous les tests ci-dessous.
			-- Permet de lib�rer les ressources n�cessaires aux tests ci-dessous.
		do
			game_library.clear_all_events
			image_file_library.quit_library
			audio_library.quit_library
			game_library.quit_library
		end

feature -- Test routines

	x_normal_test
			-- Test des cas normaux de la gestion de positionnement horizontal
		note
			testing: "covers/{HERO}.set_x", "covers/{HERO}.x", "execution/isolated", "execution/serial/graphics"
		do
			hero.set_x (123)
			assert("{HERO}.`x' normal n'est pas valide (123).", hero.x = 123)
			hero.set_x (432)
			assert("{HERO}.`x' normal n'est pas valide (432).", hero.x = 432)
		end

	x_limit_test
			-- Test les cas limites de la gestion de positionnement horizontal
		note
			testing: "covers/{HERO}.set_x", "covers/{HERO}.x", "execution/isolated", "execution/serial/graphics"
		do
			hero.set_x (123)
			hero.set_x (0)
			assert("{HERO}.`x' limite n'est pas valide.", hero.x = 0)
		end

	x_errone_test
			-- Test des cas erron�s de la gestion de positionnement horizontal
		note
			testing: "covers/{HERO}.set_x", "covers/{HERO}.x", "execution/isolated", "execution/serial/graphics"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				hero.set_x (-123)
				assert("{HERO}.`x' erron� non g�rer", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	y_normal_test
			-- Test des cas normal de la gestion de positionnement vertical
		note
			testing: "covers/{HERO}.set_y", "covers/{HERO}.y", "execution/isolated", "execution/serial/graphics"
		do
			hero.set_y (123)
			assert("{HERO}.`y' n'est pas valide (123).", hero.y = 123)
			hero.set_y (432)
			assert("{HERO}.`y' n'est pas valide (432).", hero.y = 432)
		end

	y_limit_test
			-- Test les cas limites de la gestion de positionnement vertical
		note
			testing: "covers/{HERO}.set_y", "covers/{HERO}.y", "execution/isolated", "execution/serial/graphics"
		do
			hero.set_y (123)
			hero.set_y (0)
			assert("{HERO}.`y' limite n'est pas valide.", hero.y = 0)
		end

	y_wrong_test
			-- Test des cas erron�s de la gestion de positionnement vertical
		note
			testing: "covers/{HERO}.set_y", "covers/{HERO}.y", "execution/isolated", "execution/serial/graphics"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				hero.set_y (-123)
				assert("{HERO}.`y' erron� non g�rer", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	left_update_normal_test
			-- Test des cas normal de la gestion de d�placement vers la gauche
		note
			testing: "covers/{HERO}.go_left", "covers/{HERO}.going_left", "covers/{HERO}.update", "covers/{HERO}.facing_left", "execution/isolated", "execution/serial/graphics"
		local
			l_old_x:INTEGER
		do
			hero.set_x (150)
			hero.go_right (1)
			hero.update (20)
			hero.stop_right
			assert(
					"Ne peut tester {HERO}.`go_left' car {HERO}.`go_right' n'est pas fonctionnel.",
					not hero.facing_left)
			hero.set_x (150)
			hero.go_left (40)
			assert("{HERO}.`go_left' n'est pas valide (avant {HERO}.`update').", hero.going_left)
			hero.update (60)
			assert("{HERO}.`go_left' n'est pas valide (apr�s {HERO}.`update').", hero.going_left)
			assert("{HERO}.`update' ne change pas {HERO}.`facing_left'.", hero.facing_left)
			assert("{HERO}.`update' ne change pas {HERO}.`x'.", hero.x < 150)
			l_old_x := hero.x
			hero.stop_left
			assert("{HERO}.`stop_left' n'est pas valide (apr�s {HERO}.`update').", not hero.going_left)
			hero.update (80)
			assert("{HERO}.`update' change {HERO}.`facing_left'.", hero.facing_left)
			assert("{HERO}.`update' change {HERO}.`x'.", hero.x = l_old_x)
		end

	right_update_normal_test
			-- Test des cas normal de la gestion de d�placement vers la droite
		note
			testing: "covers/{HERO}.go_right", "covers/{HERO}.going_right", "covers/{HERO}.update", "covers/{HERO}.facing_left", "execution/isolated", "execution/serial/graphics"
		local
			l_old_x:INTEGER
		do
			hero.set_x (150)
			hero.go_left (1)
			hero.update (20)
			hero.stop_left
			assert(
					"Ne peut tester {HERO}.`go_right' car {HERO}.`go_left' n'est pas fonctionnel.",
					hero.facing_left)
			hero.set_x (150)
			hero.go_right (40)
			assert("{HERO}.`go_right' n'est pas valide (avant {HERO}.`update').", hero.going_right)
			hero.update (60)
			assert("{HERO}.`go_right' n'est pas valide (apr�s {HERO}.`update').", hero.going_right)
			assert("{HERO}.`update' ne change pas {HERO}.`facing_left'.", not hero.facing_left)
			assert("{HERO}.`update' ne change pas {HERO}.`x'.", hero.x > 150)
			l_old_x := hero.x
			hero.stop_right
			assert("{HERO}.`stop_right' n'est pas valide (apr�s {HERO}.`update').", not hero.going_right)
			hero.update (80)
			assert("{HERO}.`update' change {HERO}.`facing_left'.", not hero.facing_left)
			assert("{HERO}.`update' change {HERO}.`x'.", hero.x = l_old_x)
		end

	down_update_normal_test
			-- Test des cas normal de la gestion de d�placement vers le bas
		note
			testing: "covers/{HERO}.go_down", "covers/{HERO}.going_down", "covers/{HERO}.update", "covers/{HERO}.facing_up", "execution/isolated", "execution/serial/graphics"
		local
			l_old_y:INTEGER
		do
			hero.set_y (150)
			hero.go_up (1)
			hero.update (20)
			hero.stop_up
			assert(
					"Ne peut tester {HERO}.`go_down' car {HERO}.`go_up' n'est pas fonctionnel.",
					hero.facing_up)
			hero.set_y (150)
			hero.go_down (40)
			assert("{HERO}.`go_down' n'est pas valide (avant {HERO}.`update').", hero.going_down)
			hero.update (60)
			assert("{HERO}.`go_down' n'est pas valide (apr�s {HERO}.`update').", hero.going_down)
			assert("{HERO}.`update' ne change pas {HERO}.`facing_up'.", not hero.facing_up)
			assert("{HERO}.`update' ne change pas {HERO}.`y'.", hero.y > 150)
			l_old_y := hero.y
			hero.stop_down
			assert("{HERO}.`stop_down' n'est pas valide (apr�s {HERO}.`update').", not hero.going_down)
			hero.update (80)
			assert("{HERO}.`update' change {HERO}.`facing_up'.", not hero.facing_up)
			assert("{HERO}.`update' change {HERO}.`y'.", hero.y = l_old_y)
		end

	up_update_normal_test
			-- Test des cas normal de la gestion de d�placement vers le haut
		note
			testing: "covers/{HERO}.go_up", "covers/{HERO}.going_up", "covers/{HERO}.update", "covers/{HERO}.facing_down", "execution/isolated", "execution/serial/graphics"
		local
			l_old_y:INTEGER
		do
			hero.set_y (150)
			hero.go_down (1)
			hero.update (20)
			hero.stop_down
			assert(
					"Ne peut tester {HERO}.`go_up' car {HERO}.`go_down' n'est pas fonctionnel.",
					hero.facing_down)
			hero.set_y (150)
			hero.go_up (40)
			assert("{HERO}.`go_up' n'est pas valide (avant {HERO}.`update').", hero.going_up)
			hero.update (60)
			assert("{HERO}.`go_up' n'est pas valide (apr�s {HERO}.`update').", hero.going_up)
			assert("{HERO}.`update' ne change pas {HERO}.`facing_down'.", not hero.facing_down)
			assert("{HERO}.`update' ne change pas {HERO}.`y'.", hero.y < 150)
			l_old_y := hero.y
			hero.stop_up
			assert("{HERO}.`stop_up' n'est pas valide (apr�s {HERO}.`update').", not hero.going_up)
			hero.update (80)
			assert("{HERO}.`update' change {HERO}.`facing_down'.", not hero.facing_down)
			assert("{HERO}.`update' change {HERO}.`y'.", hero.y = l_old_y)
		end

	update_limit_horizontal_test
			-- Test des cas limites de la gestion de d�placement horizontal
		note
			testing: "covers/{HERO}.update", "covers/{HERO}.facing_left", "execution/isolated", "execution/serial/graphics"
		local
			l_old_x:INTEGER
		do
			hero.set_x (150)
			hero.go_left (0)
			hero.update (0)
			assert("{HERO}.`update' change {HERO}.`x' avec un temps de 0.", hero.x = 150)
			hero.update (20)
			l_old_x := hero.x
			hero.update (20)
			assert("{HERO}.`update' change {HERO}.`x' avec un temps inchang�.", hero.x = l_old_x)
		end

	update_limit_vertical_test
			-- Test des cas limites de la gestion de d�placement vertical
		note
			testing: "covers/{HERO}.update", "covers/{HERO}.facing_down", "execution/isolated", "execution/serial/graphics"
		local
			l_old_y:INTEGER
		do
			hero.set_y (150)
			hero.go_down (0)
			hero.update (0)
			assert("{HERO}.`update' change {HERO}.`y' avec un temps de 0.", hero.y = 150)
			hero.update (20)
			l_old_y := hero.y
			hero.update (20)
			assert("{HERO}.`update' change {HERO}.`y' avec un temps inchang�.", hero.y = l_old_y)
		end

	update_wrong_1_test
			-- Test d'un cas erron�s de la gestion de d�placement
		note
			testing: "covers/{HERO}.update", "execution/isolated", "execution/serial/graphics"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				hero.go_left (10)
				hero.update (5)
				assert("{HERO}.`update' erron� non g�rer", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	update_wrong_2_test
			-- Test d'un cas erron�s de la gestion de d�placement
		note
			testing: "covers/{HERO}.update", "execution/isolated", "execution/serial/graphics"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				hero.go_left (10)
				hero.update (30)
				hero.update (20)
				assert("{HERO}.`update' erron� non g�rer", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

feature {NONE}

	window:GAME_WINDOW_RENDERED
			-- La {GAME_WINDOW} utilis� pour g�n�rer `hero`
	hero:HERO
			-- L'objet test� dans `Current'

	audio_source_factory:AUDIO_SOURCE_FACTORY
			-- Le g�n�rateur d'audio

invariant
note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[N'zu� Kouadio Ange Kouassi & Olivier M%
			%D�partement des techniques de l'informatique%
			%C�gep de Drummondville%
			%960, rue Saint-Georges%
			%Drummondville, (Qu�bec)%
			%J2C 6A2%
			%T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859%
			%Site web: http://www.cegepdrummond.ca%
			%]"
end


