note
	description: "Tests pour la classe {HERO_INHERIT_TEST}."
	generateur: "EiffelStudio test wizard"
	auteur: "N'zu� Kouadio Ange Kouassi & Olivier M"
	date: "26-05-2020"
	revision: "0.2"
	testing: "type/manual", "covers/{BANK_ACCOUNT}"

class
	HERO_INHERIT_TEST

inherit
	HERO
		undefine
			default_create
		end

	EQA_TEST_SET
		redefine
			on_prepare,
			on_clean
		end

	GAME_LIBRARY_SHARED
		undefine
			default_create
		end

	IMG_LIBRARY_SHARED
		undefine
			default_create
		end

	AUDIO_LIBRARY_SHARED
		undefine
			default_create
		end

feature {NONE} -- Events

	on_prepare
			-- Cette m�thode est lanc�e avant d'ex�cuter les tests ci-dessous
			-- Permet de g�n�rer des ressources n�cessaires � l'ex�cution des
			-- tests (sans avoir besoin de le faire dans chaque m�thode de test)
		local
			l_window_builder:GAME_WINDOW_RENDERED_BUILDER
		do
			game_library.enable_video
			image_file_library.enable_image (true, false, false)
			audio_library.enable_playback
			create l_window_builder
			l_window_builder.set_dimension(208, 208)
			window := l_window_builder.generate_window
			create audio_source_factory.make
			make(audio_source_factory)
		end

	on_clean
			-- Cette m�thode est lanc� apr�s l'ex�cution de tous les tests ci-dessous.
			-- Permet de lib�rer les ressources n�cessaires aux tests ci-dessous.
		do
			game_library.clear_all_events
			image_file_library.quit_library
			audio_library.quit_library
			game_library.quit_library
		end

feature -- Access		

	new_projectile_normal_test
			-- Test d'un cas normal de la cr�ation d'un nouveau projectile
		note
			testing: "covers/{HERO}.new_projectile", "execution/isolated", "execution/serial/graphics"
		local
			l_projectile: detachable PROJECTILE
		do
			l_projectile := projectile
			new_projectile(1)
			assert("{HERO}.`new_projectile' ne pas cr�er nouveau projectile.", attached projectile and projectile /= l_projectile)
		end

feature {NONE}

	window:GAME_WINDOW_RENDERED
			-- La {GAME_WINDOW} utilis� pour g�n�rer `hero'

	audio_source_factory:AUDIO_SOURCE_FACTORY
			-- Le g�n�rateur d'audio

invariant
note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[N'zu� Kouadio Ange Kouassi & Olivier M%
			%D�partement des techniques de l'informatique%
			%C�gep de Drummondville%
			%960, rue Saint-Georges%
			%Drummondville, (Qu�bec)%
			%J2C 6A2%
			%T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859%
			%Site web: http://www.cegepdrummond.ca%
			%]"
end
