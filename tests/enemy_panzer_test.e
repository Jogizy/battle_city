note
	description: "Tests pour la classe {ENEMY_PANZER_TEST}."
	generateur: "EiffelStudio test wizard"
	auteur: "N'zu� Kouadio Ange Kouassi & Olivier M"
	date: "26-05-2020"
	revision: "0.2"
	testing: "type/manual", "covers/{BANK_ACCOUNT}"

class
	ENEMY_PANZER_TEST

inherit
	EQA_TEST_SET
		redefine
			on_prepare,
			on_clean
		end

	GAME_LIBRARY_SHARED
		undefine
			default_create
		end

	IMG_LIBRARY_SHARED
		undefine
			default_create
		end

	AUDIO_LIBRARY_SHARED
		undefine
			default_create
		end

feature {NONE} -- Events

	on_prepare
			-- Cette m�thode est lanc�e avant d'ex�cuter les tests ci-dessous
			-- Permet de g�n�rer des ressources n�cessaires � l'ex�cution des
			-- tests (sans avoir besoin de le faire dans chaque m�thode de test)
		local
			l_window_builder:GAME_WINDOW_RENDERED_BUILDER
		do
			game_library.enable_video
			image_file_library.enable_image (true, false, false)
			audio_library.enable_playback
			create l_window_builder
			l_window_builder.set_dimension(208, 208)
			window := l_window_builder.generate_window
			create audio_source_factory.make
			create enemy_panzer.make(audio_source_factory,0)
		end

	on_clean
			-- Cette m�thode est lanc� apr�s l'ex�cution de tous les tests ci-dessous.
			-- Permet de lib�rer les ressources n�cessaires aux tests ci-dessous.
		do
			game_library.clear_all_events
			image_file_library.quit_library
			audio_library.quit_library
			game_library.quit_library
		end

feature -- Test routines

	make_normal_test
			-- Test d'un cas normal du constructeur `make'
		note
			testing: "covers/{ENEMY_PANZER}.make", "execution/isolated", "execution/serial/graphics"
		local
			l_enemy_panzer: ENEMY_PANZER
		do
			create l_enemy_panzer.make (audio_source_factory, 8)
			l_enemy_panzer.set_x (16)
			l_enemy_panzer.set_y (32)
			assert("{ENEMY_PANZER}.`x' limite est pas valide.", l_enemy_panzer.x ~ 16)
			assert("{ENEMY_PANZER}.`y' limite est pas valide.", l_enemy_panzer.y ~ 32)
		end

	x_normal_test
			-- Test des cas normaux de la gestion de positionnement horizontal
		note
			testing: "covers/{ENEMY_PANZER}.set_x", "covers/{ENEMY_PANZER}.x", "execution/isolated", "execution/serial/graphics"
		do
			enemy_panzer.set_x (100)
			assert("{ENEMY_PANZER}.`x' normal n'est pas valide (100).", enemy_panzer.x = 100)
			enemy_panzer.set_x (176)
			assert("{ENEMY_PANZER}.`x' normal n'est pas valide (176).", enemy_panzer.x = 176)
		end

	x_limit_test
			-- Test les cas limites de la gestion de positionnement horizontal
		note
			testing: "covers/{ENEMY_PANZER}.set_x", "covers/{ENEMY_PANZER}.x", "execution/isolated", "execution/serial/graphics"
		do
			enemy_panzer.set_x (100)
			enemy_panzer.set_x (0)
			assert("{ENEMY_PANZER}.`x' limite n'est pas valide.", enemy_panzer.x = 0)
		end

	x_wrong_test
			-- Test des cas erron�s de la gestion de positionnement horizontal
		note
			testing: "covers/{ENEMY_PANZER}.set_x", "covers/{ENEMY_PANZER}.x", "execution/isolated", "execution/serial/graphics"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				enemy_panzer.set_x (-100)
				assert("{ENEMY_PANZER}.`x' erron� non g�rer", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	y_normal_test
			-- Test des cas normal de la gestion de positionnement vertical
		note
			testing: "covers/{ENEMY_PANZER}.set_y", "covers/{ENEMY_PANZER}.y", "execution/isolated", "execution/serial/graphics"
		do
			enemy_panzer.set_y (96)
			assert("{ENEMY_PANZER}.`y' n'est pas valide (96.", enemy_panzer.y = 96)
			enemy_panzer.set_y (160)
			assert("{ENEMY_PANZER}.`y' n'est pas valide (160).", enemy_panzer.y = 160)
		end

	y_limit_test
			-- Test les cas limites de la gestion de positionnement vertical
		note
			testing: "covers/{ENEMY_PANZER}.set_y", "covers/{ENEMY_PANZER}.y", "execution/isolated", "execution/serial/graphics"
		do
			enemy_panzer.set_y (96)
			enemy_panzer.set_y (0)
			assert("{ENEMY_PANZER}.`y' limite n'est pas valide.", enemy_panzer.y = 0)
		end

	y_wrong_test
			-- Test des cas erron�s de la gestion de positionnement vertical
		note
			testing: "covers/{ENEMY_PANZER}.set_y", "covers/{ENEMY_PANZER}.y", "execution/isolated", "execution/serial/graphics"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				enemy_panzer.set_y (-96)
				assert("{ENEMY_PANZER}.`y' erron� non g�rer", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	left_update_normal_test
			-- Test des cas normal de la gestion de d�placement vers la gauche
		note
			testing: "covers/{ENEMY_PANZER}.go_left", "covers/{ENEMY_PANZER}.going_left", "covers/{ENEMY_PANZER}.update", "covers/{ENEMY_PANZER}.facing_left", "execution/isolated", "execution/serial/graphics"
		do
			enemy_panzer.set_x (64)
			enemy_panzer.go_right (1)
			enemy_panzer.update (20)
			assert(
					"Ne peut tester {ENEMY_PANZER}.`go_left' car {ENEMY_PANZER}.`go_right' n'est pas fonctionnel.",
					not enemy_panzer.facing_left)
			enemy_panzer.set_x (64)
			enemy_panzer.go_left (40)
			assert("{ENEMY_PANZER}.`go_left' n'est pas valide (avant {ENEMY_PANZER}.`update').", enemy_panzer.going_left)
			enemy_panzer.update (80)
			assert("{ENEMY_PANZER}.`go_left' n'est pas valide (apr�s {ENEMY_PANZER}.`update').", enemy_panzer.going_left)
			assert("{ENEMY_PANZER}.`update' ne change pas {ENEMY_PANZER}.`facing_left'.", enemy_panzer.facing_left)
			assert("{ENEMY_PANZER}.`update' ne change pas {ENEMY_PANZER}.`x'.", enemy_panzer.x < 64)
		end

	right_update_normal_test
			-- Test des cas normal de la gestion de d�placement vers la droite
		note
			testing: "covers/{ENEMY_PANZER}.go_right", "covers/{ENEMY_PANZER}.going_right", "covers/{ENEMY_PANZER}.update", "covers/{ENEMY_PANZER}.facing_left", "execution/isolated", "execution/serial/graphics"
		do
			enemy_panzer.set_x (64)
			enemy_panzer.go_left (1)
			enemy_panzer.update (20)
			assert(
					"Ne peut tester {ENEMY_PANZER}.`go_right' car {ENEMY_PANZER}.`go_left' n'est pas fonctionnel.",
					enemy_panzer.facing_left)
			enemy_panzer.set_x (64)
			enemy_panzer.go_right (40)
			assert("{ENEMY_PANZER}.`go_right' n'est pas valide (avant {ENEMY_PANZER}.`update').", enemy_panzer.going_right)
			enemy_panzer.update (80)
			assert("{ENEMY_PANZER}.`go_right' n'est pas valide (apr�s {ENEMY_PANZER}.`update').", enemy_panzer.going_right)
			assert("{ENEMY_PANZER}.`update' ne change pas {ENEMY_PANZER}.`facing_left'.", not enemy_panzer.facing_left)
			assert("{ENEMY_PANZER}.`update' ne change pas {ENEMY_PANZER}.`x'.", enemy_panzer.x > 64)
		end

	down_update_normal_test
			-- Test des cas normal de la gestion de d�placement vers le bas
		note
			testing: "covers/{ENEMY_PANZER}.go_down", "covers/{ENEMY_PANZER}.going_down", "covers/{ENEMY_PANZER}.update", "covers/{ENEMY_PANZER}.facing_up", "execution/isolated", "execution/serial/graphics"
		do
			enemy_panzer.set_y (64)
			enemy_panzer.go_up (1)
			enemy_panzer.update (20)
			assert(
					"Ne peut tester {ENEMY_PANZER}.`go_down' car {ENEMY_PANZER}.`go_up' n'est pas fonctionnel.",
					enemy_panzer.facing_up)
			enemy_panzer.set_y (64)
			enemy_panzer.go_down (40)
			assert("{ENEMY_PANZER}.`go_down' n'est pas valide (avant {ENEMY_PANZER}.`update').", enemy_panzer.going_down)
			enemy_panzer.update (80)
			assert("{ENEMY_PANZER}.`go_down' n'est pas valide (apr�s {ENEMY_PANZER}.`update').", enemy_panzer.going_down)
			assert("{ENEMY_PANZER}.`update' ne change pas {ENEMY_PANZER}.`facing_up'.", not enemy_panzer.facing_up)
			assert("{ENEMY_PANZER}.`update' ne change pas {ENEMY_PANZER}.`y'.", enemy_panzer.y > 64)
		end

	up_update_normal_test
			-- Test des cas normal de la gestion de d�placement vers le haut
		note
			testing: "covers/{ENEMY_PANZER}.go_up", "covers/{ENEMY_PANZER}.going_up", "covers/{ENEMY_PANZER}.update", "covers/{ENEMY_PANZER}.facing_down", "execution/isolated", "execution/serial/graphics"
		do
			enemy_panzer.set_y (64)
			enemy_panzer.go_down (1)
			enemy_panzer.update (20)
			assert(
					"Ne peut tester {ENEMY_PANZER}.`go_up' car {ENEMY_PANZER}.`go_down' n'est pas fonctionnel.",
					enemy_panzer.facing_down)
			enemy_panzer.set_y (64)
			enemy_panzer.go_up (40)
			assert("{ENEMY_PANZER}.`go_up' n'est pas valide (avant {ENEMY_PANZER}.`update').", enemy_panzer.going_up)
			enemy_panzer.update (80)
			assert("{ENEMY_PANZER}.`go_up' n'est pas valide (apr�s {ENEMY_PANZER}.`update').", enemy_panzer.going_up)
			assert("{ENEMY_PANZER}.`update' ne change pas {ENEMY_PANZER}.`facing_down'.", not enemy_panzer.facing_down)
			assert("{ENEMY_PANZER}.`update' ne change pas {ENEMY_PANZER}.`y'.", enemy_panzer.y < 64)
		end

	update_limit_horizontal_test
			-- Test des cas limites de la gestion de d�placement horizontal
		note
			testing: "covers/{ENEMY_PANZER}.update", "covers/{ENEMY_PANZER}.facing_left", "execution/isolated", "execution/serial/graphics"
		local
			l_old_x:INTEGER
		do
			enemy_panzer.set_x (64)
			enemy_panzer.go_left (0)
			enemy_panzer.update (0)
			assert("{ENEMY_PANZER}.`update' change {ENEMY_PANZER}.`x' avec un temps de 0.", enemy_panzer.x = 64)
			enemy_panzer.update (20)
			l_old_x := enemy_panzer.x
			enemy_panzer.update (20)
			assert("{ENEMY_PANZER}.`update' change {ENEMY_PANZER}.`x' avec un temps inchang�.", enemy_panzer.x = l_old_x)
		end

	update_limit_vertical_test
			-- Test des cas limites de la gestion de d�placement vertical
		note
			testing: "covers/{ENEMY_PANZER}.update", "covers/{ENEMY_PANZER}.facing_down", "execution/isolated", "execution/serial/graphics"
		local
			l_old_y:INTEGER
		do
			enemy_panzer.set_y (64)
			enemy_panzer.go_down (0)
			enemy_panzer.update (0)
			assert("{ENEMY_PANZER}.`update' change {ENEMY_PANZER}.`y' avec un temps de 0.", enemy_panzer.y = 64)
			enemy_panzer.update (20)
			l_old_y := enemy_panzer.y
			enemy_panzer.update (20)
			assert("{ENEMY_PANZER}.`update' change {ENEMY_PANZER}.`y' avec un temps inchang�.", enemy_panzer.y = l_old_y)
		end

	update_wrong_1_test
			-- Test d'un cas erron�s de la gestion de d�placement
		note
			testing: "covers/{ENEMY_PANZER}.update", "execution/isolated", "execution/serial/graphics"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				enemy_panzer.go_left (10)
				enemy_panzer.update (5)
				assert("{ENEMY_PANZER}.`update' erron� non g�rer", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	update_wrong_2_test
			-- Test d'un cas erron�s de la gestion de d�placement
		note
			testing: "covers/{ENEMY_PANZER}.update", "execution/isolated", "execution/serial/graphics"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				enemy_panzer.go_left (10)
				enemy_panzer.update (40)
				enemy_panzer.update (20)
				assert("{ENEMY_PANZER}.`update' erron� non g�rer", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	stop_right_wrong_test
			-- Test cas erron� du stop vers la droite `stop_right'
		note
			testing: "covers/{ENEMY_PANZER}.stop_right", "execution/isolated", "execution/serial/graphics"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				enemy_panzer.stop_right
				assert("{ENEMY_PANZER}.`stop_right' erron� non g�rer", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	stop_left_wrong_test
			-- Test cas erron� du stop vers la gauche`stop_left'
		note
			testing: "covers/{ENEMY_PANZER}.stop_left", "execution/isolated", "execution/serial/graphics"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				enemy_panzer.stop_left
				assert("{ENEMY_PANZER}.`stop_left' erron� non g�rer", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	 stop_up_wrong_test
	 		-- Test cas erron� du stop vers le haut `stop_up'
		note
			testing: "covers/{ENEMY_PANZER}.stop_up", "execution/isolated", "execution/serial/graphics"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				enemy_panzer.stop_right
				assert("{ENEMY_PANZER}.`stop_up' erron� non g�rer", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	stop_down_wrong_test
			-- Test cas erron� du stop vers le bas `stop_down'
		note
			testing: "covers/{ENEMY_PANZER}.stop_down", "execution/isolated", "execution/serial/graphics"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				enemy_panzer.stop_right
				assert("{ENEMY_PANZER}.`stop_down' erron� non g�rer", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

feature {NONE}

	window:GAME_WINDOW_RENDERED
			-- La {GAME_WINDOW} utilis� pour g�n�rer `enemy_panzer`

	enemy_panzer:ENEMY_PANZER
			-- L'objet test� dans `Current'

	audio_source_factory:AUDIO_SOURCE_FACTORY
			-- Le g�n�rateur d'audio

invariant
note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[N'zu� Kouadio Ange Kouassi & Olivier M%
			%D�partement des techniques de l'informatique%
			%C�gep de Drummondville%
			%960, rue Saint-Georges%
			%Drummondville, (Qu�bec)%
			%J2C 6A2%
			%T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859%
			%Site web: http://www.cegepdrummond.ca%
			%]"
end
