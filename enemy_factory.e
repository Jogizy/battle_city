note
	description: "Classe permettant la g�n�ration des ennemis."
	auteur: "N'zu� Kouadio Ange Kouassi & Olivier M"
	date: "2020-04-14"
	revision: "1.0"

class
	ENEMY_FACTORY

inherit
	DISPLAY_FACTORY

create
	make

feature {NONE} -- Initialisation

	make(a_audio_factory:AUDIO_SOURCE_FACTORY)
			-- Initialisation de `Current' � utiliser avec `a_audio_factory'
		do
			audio_factory := a_audio_factory
		end

feature -- Acc�s

	new_char(a_x, a_y:INTEGER; a_timestamp:NATURAL_32):ENEMY_CHAR
			-- Cr�ation d'un nouveau char en fonction de ses coordonn�es `a_x' et `a_y'
			-- Retourne une unit� ennemi de type char
		do
			create Result.make(audio_factory,a_timestamp)
			set_position(a_x, a_y, Result)
		end

	new_panzer(a_x, a_y:INTEGER; a_timestamp:NATURAL_32):ENEMY_PANZER
			-- Cr�ation d'un nouveau panzer en fonction de ses coordonn�es `a_x' et `a_y'
			-- Retourne une unit� ennemi de type panzer
		do
			create Result.make(audio_factory,a_timestamp)
			set_position(a_x, a_y, Result)
		end

	new_tank(a_x, a_y:INTEGER; a_timestamp:NATURAL_32):ENEMY_TANK
			-- Cr�ation d'un nouveau tank en fonction de ses coordonn�es `a_x' et `a_y'
			-- Retourne une unit� ennemi de type tank
		do
			create Result.make(audio_factory,a_timestamp)
			set_position(a_x, a_y, Result)
		end

feature {NONE} -- Impl�mentation

	audio_factory:AUDIO_SOURCE_FACTORY
			-- Le g�n�rateur audio

invariant
note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
			N'zu� Kouadio Ange Kouassi & Olivier M
			D�partement des techniques de l'informatique
			C�gep de Drummondville
			960, rue Saint-Georges
			Drummondville, (Qu�bec)
			J2C 6A2
			T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859
			Site web: http://www.cegepdrummond.ca
		]"
end
