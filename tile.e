note
	description: "Classe abstraite de tuile."
	auteur: "N'zu� Kouadio Ange Kouassi & Olivier M"
	date: "2020-04-14"
	revision: "1.0"

deferred class
	TILE

inherit
	DISPLAY

feature {NONE} -- Initialisation

	make
			-- Initialisation de `Current'
		do
			initialize_tile_coordinate
			sub_image_width := 16
			sub_image_height := 16
		end

	initialize_tile_coordinate
			-- Initialisation des coordonn�es de `Current'
		deferred
		end

note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
			N'zu� Kouadio Ange Kouassi & Olivier M
			D�partement des techniques de l'informatique
			C�gep de Drummondville
			960, rue Saint-Georges
			Drummondville, (Qu�bec)
			J2C 6A2
			T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859
			Site web: http://www.cegepdrummond.ca
		]"
end
