note
	description: "Affichage de texte en fonction d'une �tape produite dans le jeu ."
	auteur: "N'zu� Kouadio Ange Kouassi & Olivier M"
	date: "2020-04-14"
	revision: "1.0"

class
	TEXT_POSTER

inherit
	GAME_LIBRARY_SHARED
	ENGINE_TEXT
		rename
			make as make_engine_text
		end

create
	make

feature {NONE} -- Initialisation

	make(a_window:GAME_WINDOW_RENDERED; a_font_size: INTEGER; a_text:STRING; a_red_code,a_green_code,a_blue_code:NATURAL_8)
			-- Initialisation de `Current' � utiliser avec `a_window',
			--`a_text',`a_red_code', `a_green_code', `a_blue_code'
		do
			font_size:= a_font_size
			message := a_text
			red_code := a_red_code
			green_code := a_green_code
			blue_code := a_blue_code
			make_engine_text(a_window)
			initialize_font
			initialize_text
			is_quit := False
		end

	initialize_font
			-- Initialise la police texte
		do
			font := load_font("font.ttf",font_size)
		end

	initialize_text
			-- Initialise le text de `current'
		do
			texts.extend ([40, 80, message, create {GAME_COLOR}.make_rgb(red_code,green_code,blue_code),font])
		end

feature -- Acc�s

	run
			-- Lance la visionneuse du jeu
		do
			game_library.quit_signal_actions.extend(agent on_quit)
			window.key_pressed_actions.extend(agent on_key_pressed)
			game_library.iteration_actions.extend(agent on_iteration)
			if window.renderer.driver.is_present_synchronized_supported then
				game_library.launch_no_delay
			else
				game_library.launch
			end
			game_library.clear_all_events
		end

	on_iteration(a_timestamp:NATURAL_32)
			-- �v�nement qui est lanc� � chaque it�ration.
		do
			-- Redessine la sc�ne
			window.renderer.set_drawing_color(create {GAME_COLOR}.make_rgb (160, 160,160)) -- Couleur de l'arri�re plan
			window.renderer.draw_filled_rectangle(0, 0, texture.width, texture.height)
			draw_texts
			-- Met � jour la modification � l'�cran
			window.renderer.present
		end


	on_key_pressed(a_timestamp:NATURAL_32; a_key_event:GAME_KEY_EVENT)
			-- Action lorsqu'une touche du clavier a �t� enfonc�e
		do
			if not a_key_event.is_repeat then
				if a_key_event.is_c then
					game_library.stop
				end
			end
		end


	on_quit(a_timestamp: NATURAL_32)
			-- Cette m�thode est appel�e lorsque le signal de sortie est envoy�
			-- � l'application (ex: bouton X de la fen�tre).
		do
			-- Arr�te la boucle du contr�leur (autorise game_library.launch � revenir)
			is_quit := True
			game_library.stop
		end


	font:TEXT_FONT
			-- La police utilis�e pour dessiner du texte

	red_code,green_code,blue_code:NATURAL_8
			-- Les codes couleurs

  	font_size:INTEGER
  			-- La taille de la police

	message:STRING
			-- Le message � afficher dans `a_window'

	is_quit:BOOLEAN
			-- vrai si c'est quitt�, sinon faux


invariant
note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
			N'zu� Kouadio Ange Kouassi & Olivier M
			D�partement des techniques de l'informatique
			C�gep de Drummondville
			960, rue Saint-Georges
			Drummondville, (Qu�bec)
			J2C 6A2
			T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859
			Site web: http://www.cegepdrummond.ca
		]"
end
