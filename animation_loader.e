note
	description: "Classe permettant de charger l'animation des objets"
	auteur: "N'zu� Kouadio Ange Kouassi & Olivier M"
	date: "2020-05-06"
	revision: "1.0"

class
	ANIMATION_LOADER

inherit
	DISPLAY
		redefine
			make_from_other
		end

create
	make_from_other

feature {NONE} -- Initialisation

	make_from_other(a_other:like current)
			-- Recup�re les attributs de `a_other' et assigne les attributs de `Current'
		do
			precursor(a_other)
			make
			moving := a_other.moving
			going_left := a_other.going_left
			going_right := a_other.going_right
			going_down := a_other.going_down
			going_up := a_other.going_up
			facing_right := a_other.facing_right
			facing_left := a_other.facing_left
			facing_down := a_other.facing_down
			facing_up := a_other.facing_up
		end

	make
			-- Initialisation de `Current'
		do
			create {ARRAYED_LIST[TUPLE[x,y:INTEGER]]} animation_coordinates_vertical.make(2)
			create {ARRAYED_LIST[TUPLE[x,y:INTEGER]]} animation_coordinates_horizontal.make(2)
		end

feature -- Acc�s

	moving:INTEGER
			-- Le d�placement de `Current'

	going_left:BOOLEAN
			-- Est-ce que `Current' se d�place vers la gauche

	going_right:BOOLEAN
			-- Est-ce que `Current' se d�place vers la droite

	going_down:BOOLEAN
			-- Est-ce que `Current'" se d�place vers le bas

	going_up:BOOLEAN
			-- Est-ce que `Current' se d�place vers le haut

	facing_right:BOOLEAN
			-- Est-ce que `Current' fait face vers la droite

	facing_left:BOOLEAN
			-- Est-ce que `Current' fait face vers la gauche

	facing_up:BOOLEAN
			-- Est-ce que `Current' fait face vers le haut

	facing_down:BOOLEAN
			-- Est-ce que `Current' fait face vers le bas

feature {NONE} -- Impl�mentation

	animation_coordinates_horizontal:LIST[TUPLE[x,y:INTEGER]]
			-- Chaque coordonn�e horizontale d'une partie des images en `surface'

	animation_coordinates_vertical:LIST[TUPLE[x,y:INTEGER]]
			-- Chaque coordonn�e verticale d'une portion d'images en `surface`

invariant
note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
			N'zu� Kouadio Ange Kouassi & Olivier M
			D�partement des techniques de l'informatique
			C�gep de Drummondville
			960, rue Saint-Georges
			Drummondville, (Qu�bec)
			J2C 6A2
			T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859
			Site web: http://www.cegepdrummond.ca
		]"
end
