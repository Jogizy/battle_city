note
	description: "L'unit� ennemi de type CHAR"
	auteur: "N'zu� Kouadio Ange Kouassi & Olivier M"
	date: "2020-04-14"
	revision: "1.0"

class
	ENEMY_CHAR

inherit
	ENEMY

create
	make

feature {NONE} -- Initialisation

	initialize_animation_coordinate
			-- Initialisation des coordonn�es d'animation de `Current'
		do
			animation_coordinates_vertical.extend([128,80])
			animation_coordinates_vertical.extend([144,80])

			animation_coordinates_horizontal.extend([224,80])
			animation_coordinates_horizontal.extend([240,80])
		end

	initialize_speed
			-- Initialisation de la vitesse de d�placement de `Current'
		do
			movement_delta := 30
			animation_delta := 200
		end

	initialize_hitpoints
			-- Initialisation du nombre de points de vies de `Current'
		do
			hitpoints := 2
		end

invariant
note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
			N'zu� Kouadio Ange Kouassi & Olivier M
			D�partement des techniques de l'informatique
			C�gep de Drummondville
			960, rue Saint-Georges
			Drummondville, (Qu�bec)
			J2C 6A2
			T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859
			Site web: http://www.cegepdrummond.ca
		]"
end
