note
	description: "Cette classe permet de jouer et d'arr�ter la musique."
	auteur: "N'zu� Kouadio Ange Kouassi & Olivier M"
	date: "2020-04-14"
	revision: "2.0"

class
	MUSIC

create
	make

feature {NONE} -- Initialisation

	make(a_audio_factory:AUDIO_SOURCE_FACTORY)
			-- Initialisation de `Current' � utiliser avec `a_audio_factory'
		do
			create music.make(a_audio_factory.source_music, "music_intro.ogg")
			a_audio_factory.source_music.set_gain(0.5)
			music_state := False
		end

	music:AUDIO
			-- La musique principal

	music_state:BOOLEAN
			-- L'�tat de la musique (vrai si audible, sinon faux)

feature -- Acc�s

	toggle_music
			-- Change l'�tat de la musique
		do
			if music_state then
				music.stop
			else
				music.play(-1)
			end
			music_state := not music_state
		end

	play
			-- Joue la musique
		do
			music.play(-1)
			music_state := True
		end

note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
			N'zu� Kouadio Ange Kouassi & Olivier M
			D�partement des techniques de l'informatique
			C�gep de Drummondville
			960, rue Saint-Georges
			Drummondville, (Qu�bec)
			J2C 6A2
			T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859
			Site web: http://www.cegepdrummond.ca
		]"
end
