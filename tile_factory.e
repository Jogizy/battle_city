note
	description: "Classe permettant la g�n�ration des tuiles."
	auteur: "N'zu� Kouadio Ange Kouassi & Olivier M"
	date: "2020-04-14"
	revision: "1.0"

class
	TILE_FACTORY

inherit
	DISPLAY_FACTORY

create
	make

feature {NONE} -- Initialisation

	make(a_audio_factory:AUDIO_SOURCE_FACTORY)
			-- Initialisation de `Current' � utiliser avec `a_audio_factory'
		do
			audio_factory := a_audio_factory
		end

feature -- Acc�s

	new_brick(a_x, a_y:INTEGER):TILE_BRICK
			-- Cr�ation d'un nouveau {TILE_BRICK} en fonction de ses coordonn�es `a_x' et `a_y'
			-- Retourne un {TILE_BRICK}
		do
			create Result.make(audio_factory)
			set_position(a_x, a_y, Result)
		end

	new_steel(a_x, a_y:INTEGER):TILE_STEEL
			-- Cr�ation d'un nouveau {TILE_STEEL} en fonction de ses coordonn�es `a_x' et `a_y'
			-- Retourne un {TILE_STEEL}
		do
			create Result.make(audio_factory)
			set_position(a_x, a_y, Result)
		end

	new_water(a_x, a_y:INTEGER):TILE_WATER
			-- Cr�ation d'un nouveau {TILE_WATER} en fonction de ses coordonn�es `a_x' et `a_y'
			-- Retourne un {TILE_WATER}
		do
			create Result.make(audio_factory)
			set_position(a_x, a_y, Result)
		end

	new_grass(a_x, a_y:INTEGER):TILE_GRASS
			-- Cr�ation d'un nouveau {TILE_GRASS}  en fonction de ses coordonn�es `a_x' et `a_y'
			-- Retourne un {TILE_GRASS}
		do
			create Result.make
			set_position(a_x, a_y, Result)
		end

	new_king(a_x, a_y:INTEGER):TILE_KING
			-- Cr�ation d'un nouveau {TILE_KING}  en fonction de ses coordonn�es `a_x' et `a_y'
			-- Retourne un {TILE_KING}
		do
			create Result.make(audio_factory)
			set_position(a_x, a_y, Result)
		end

feature {NONE} -- Impl�mentation

	audio_factory:AUDIO_SOURCE_FACTORY
			-- Le g�n�rateur audio

invariant
note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
			N'zu� Kouadio Ange Kouassi & Olivier M
			D�partement des techniques de l'informatique
			C�gep de Drummondville
			960, rue Saint-Georges
			Drummondville, (Qu�bec)
			J2C 6A2
			T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859
			Site web: http://www.cegepdrummond.ca
		]"
end
