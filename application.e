note
	description: "Classe racine pour le jeu"
	auteur: "Olivier Meloche & N'zu� Kouadio Ange Kouassi"
	date_origine: "2017-01-13"
	date: "2020-05-04"
	revision: "3.0"

class
	APPLICATION

inherit
	ANY
	GAME_LIBRARY_SHARED
	IMG_LIBRARY_SHARED
	TEXT_LIBRARY_SHARED
	AUDIO_LIBRARY_SHARED
	ARGUMENTS

create
	make_single,
	make_server,
	make_customer

feature {NONE} -- Initialisation

	make(a_viewer:BOOLEAN)
			-- Ex�cute l'application. Si `a_viewer ', cr�e un {VIEWER_ENGINE} � la place
			-- de {GAME_NETWORK_ENGINE}
		local
			l_router:detachable ROUTER
		do
			game_library.enable_video
			-- Active l'image PNG (mais pas TIF ou JPG).
			image_file_library.enable_image (true, false, false)
			-- Active les fonctionnalit�s audio
			audio_library.enable_playback
			-- Active les fonctionnalit�s du texte
			text_library.enable_text
			-- Masque le curseur
			game_library.hide_mouse_cursor

			create l_router.make(socket)
			if not l_router.has_error then
				if a_viewer and attached socket as la_socket then
					l_router.run_viewer
				else
					l_router.run
				end
			end
			l_router := Void
			game_library.clear_all_events
			text_library.quit_library
			image_file_library.quit_library
			audio_library.quit_library
			game_library.quit_library
		end

	make_server
			-- Ex�cute l'application r�seau de la visionneuse
		local
			l_port:INTEGER
			l_serveur_socket:NETWORK_STREAM_SOCKET
			l_client_socket:NETWORK_STREAM_SOCKET
		do
			l_port := 12345
			create l_serveur_socket.make_server_by_port(l_port)
			if not l_serveur_socket.is_bound then
				io.put_string ("Impossible de r�server le port " + l_port.out + ".%N")
			else
				l_serveur_socket.listen(1)
				l_serveur_socket.accept
				l_client_socket := l_serveur_socket.accepted
				if attached l_client_socket then
					socket := l_client_socket
					make(False)
					l_client_socket.close
				else
					io.put_string ("Impossible de connecter le client.%N")
				end
			end
			l_serveur_socket.close
		end

	make_single
			-- Ex�cute l'application � application unique (pas de r�seau)
		do
			make(False)
		end

	make_customer
			-- Ex�cute l'application r�seau de jeu
		local
			l_addr_factory:INET_ADDRESS_FACTORY
			l_inet_address:detachable INET_ADDRESS
			l_socket: NETWORK_STREAM_SOCKET
			l_adresse:STRING
			l_port:INTEGER
		do
			create l_addr_factory
			l_port := 12345
			if argument_count > 0 then
				l_adresse := argument(1)
				l_inet_address := l_addr_factory.create_from_name(l_adresse)
				if attached l_inet_address as la_inet_adresse then
					create l_socket.make_client_by_address_and_port(la_inet_adresse, l_port)
					if l_socket.invalid_address then
						io.put_string("Ne peut pas se connecter � l'adresse " + l_adresse + ":" + l_port.out +".%N")
					else
						l_socket.connect
						if not l_socket.is_connected then
							io.put_string("Ne peut pas se connecter � l'adresse " + l_adresse + ":" + l_port.out +".%N")
						else
							socket := l_socket
							make(True)
							l_socket.close
						end
					end
				else
					io.put_string("Erreur: Adresse " + l_adresse + " non reconnue!%N")
				end
			else
				make(False)
			end
		end

feature {NONE} -- Impl�mentation

	socket:detachable NETWORK_STREAM_SOCKET
			-- Le socket pour communiquer entre le {GAME_NETWORK_ENGINE} et le {VIEWER_ENGINE}.

invariant
note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
			N'zu� Kouadio Ange Kouassi & Olivier M
			D�partement des techniques de l'informatique
			C�gep de Drummondville
			960, rue Saint-Georges
			Drummondville, (Qu�bec)
			J2C 6A2
			T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859
			Site web: http://www.cegepdrummond.ca
		]"
end
