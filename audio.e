note
	description: "Cette classe lit les fichiers audios pass�s en argument."
	auteur: "Olivier Meloche & N'zu� Kouadio Ange Kouassi"
	date: "2020-02-24"
	revision: "2.0"

class
	AUDIO

inherit
	AUDIO_LIBRARY_SHARED

create
	make

feature {NONE} -- Initialisation

	make(a_source:AUDIO_SOURCE; a_path:STRING)
			-- Initialisation de `Current ' � utiliser avec `a_source' et `a_path'
		do
			source := a_source
			create sound.make(a_path)
			sound.open
			source.gain := 0.5
		end

feature -- Acc�s

	play(a_number:INTEGER)
			-- Joue le son selon la source d�sir�
			-- 'a_nombre' le nombre de fois que le son se r�pete (-1 = infini)
		do
			source.stop
			source.queue_sound_loop(sound, a_number)
			source.play
		end

	stop
			-- Arr�te la lecture du son
		do
			source.stop
		end

feature {NONE} -- Impl�mentation

	source:AUDIO_SOURCE
			-- La source audio � ajouter

	sound:AUDIO_SOUND_FILE
			-- Le fichier audio

invariant
note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
			N'zu� Kouadio Ange Kouassi & Olivier M
			D�partement des techniques de l'informatique
			C�gep de Drummondville
			960, rue Saint-Georges
			Drummondville, (Qu�bec)
			J2C 6A2
			T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859
			Site web: http://www.cegepdrummond.ca
		]"
end

