note
	description: "Classe abstraite permettant l'animation des objets"
	auteur: "N'zu� Kouadio Ange Kouassi & Olivier M"
	date: "2020-04-14"
	revision: "2.0"

deferred class
	ANIMATION

inherit
	ANIMATION_LOADER
		rename
			make as make_loader
		end

feature {NONE} -- Initialisation		
	make
			-- Initialisation de `Current'
		do
			make_loader
			initialize_animation_coordinate
			sub_image_x := animation_coordinates_vertical.first.x
			sub_image_y := animation_coordinates_vertical.first.y
			sub_image_x := animation_coordinates_horizontal.first.x
			sub_image_y := animation_coordinates_horizontal.first.y
			initialize_speed
			facing_right := True
			can_stop := True
		end

	initialize_animation_coordinate
			-- Initialise l'animation horizontal `animation_coordinates_horizontal'
			-- et l'animation vertical `animation_coordinates_vertical'
		deferred
		end

	initialize_speed
			-- Initialise la vitesse de d�placement
		deferred
		end

feature -- Acc�s

	update(a_timestamp:NATURAL_32)
			-- Mets � jour la surface en fonction de l'actuel `a_timestamp'.
			-- Toutes les 100 ms, l'image change; chaque courant de 10 ms se d�place
		require
			Timestamp_Valid: a_timestamp >= old_timestamp
		local
			l_coordinate:TUPLE[x,y:INTEGER]
			l_delta_time:NATURAL_32
		do
			l_delta_time := a_timestamp - old_timestamp
			moving := (l_delta_time // movement_delta).to_integer_32
			if going_left or going_right then
				l_coordinate := animation_coordinates_horizontal.at((((a_timestamp // animation_delta) \\
										animation_coordinates_horizontal.count.to_natural_32) + 1).to_integer_32)
				sub_image_x := l_coordinate.x
				sub_image_y := l_coordinate.y
				movement_horizontal(l_delta_time)
			elseif going_down or going_up then
				l_coordinate := animation_coordinates_vertical.at((((a_timestamp // animation_delta) \\
										animation_coordinates_vertical.count.to_natural_32) + 1).to_integer_32)
				sub_image_x := l_coordinate.x
				sub_image_y := l_coordinate.y
				movement_vertical(l_delta_time)
			end
		end

	movement_horizontal(a_delta_time:NATURAL_32)
			-- Deplacement horizontal de `Current'
		do
			if a_delta_time // movement_delta > 0 then
				if going_right then
					x := x + moving
				else
					x := x - moving
				end
				old_timestamp := old_timestamp + (a_delta_time // movement_delta) * movement_delta
			end
		end

	movement_vertical(a_delta_time:NATURAL_32)
			-- Deplacement vertical de `Current'
		do
			if a_delta_time // movement_delta > 0 then
				if going_up then
					y := y - moving
				else
					y := y + moving
				end
				old_timestamp := old_timestamp + (a_delta_time // movement_delta) * movement_delta
			end
		end

	go_left(a_timestamp:NATURAL_32)
			-- Faire `Current' commencer � se d�placer vers la gauche
		do
			old_timestamp := a_timestamp
			going_left := True
			facing_left := True
			facing_right := False
			facing_down := False
			facing_up := False
		ensure
			valid_going: going_left
			valid_facing_left: facing_left
			valid_facing_right: not facing_right
			valid_facing_down: not facing_down
			valid_facing_up: not facing_up
		end

	go_right(a_timestamp:NATURAL_32)
			-- Faire `Current' commencer � se d�placer vers la droite
		do
			old_timestamp := a_timestamp
			going_right := True
			facing_left := False
			facing_right := True
			facing_down := False
			facing_up := False
		ensure
			valid_going: going_right
			valid_facing_left: not facing_left
			valid_facing_right: facing_right
			valid_facing_down: not facing_down
			valid_facing_up: not facing_up
		end

	go_up(a_timestamp:NATURAL_32)
			-- Faire `Current' commencer � se d�placer vers le haut
		do
			old_timestamp := a_timestamp
			going_up := True
			facing_left := False
			facing_right := False
			facing_down := False
			facing_up := True
		ensure
			valid_going: going_up
			valid_facing_left: not facing_left
			valid_facing_right: not facing_right
			valid_facing_down: not facing_down
			valid_facing_up: facing_up
		end

	go_down(a_timestamp:NATURAL_32)
			-- Faire `Current' commencer � se d�placer vers le bas
		do
			old_timestamp := a_timestamp
			going_down := True
			facing_left := False
			facing_right := False
			facing_down := True
			facing_up := False
		ensure
			valid_going: going_down
			valid_facing_left: not facing_left
			valid_facing_right: not facing_right
			valid_facing_down: facing_down
			valid_facing_up: not facing_up
		end

	stop_left
			-- Arr�te le `Current' de se d�placer vers la gauche
		require
			Valid_stop: can_stop
		do
			going_left := False
			if not going_right then
				sub_image_x := animation_coordinates_horizontal.first.x
				sub_image_y := animation_coordinates_horizontal.first.y
			end
		ensure
			valid_going: not going_left
		end

	stop_right
			-- Arr�te le `Current' de se d�placer vers la droite
		require
			Valid_stop: can_stop
		do
			going_right := False
			if not going_left then
				sub_image_x := animation_coordinates_horizontal.first.x
				sub_image_y := animation_coordinates_horizontal.first.y
			end
		ensure
			valid_going: not going_right
		end

	stop_up
			-- Arr�te le `Current' de se d�placer vers le haut
		require
			Valid_stop: can_stop
		do
			going_up := False
			if not going_down then
				sub_image_x := animation_coordinates_vertical.first.x
				sub_image_y := animation_coordinates_vertical.first.y
			end
		ensure
			valid_going: not going_up
		end

	stop_down
			-- Arr�te le `Current' de se d�placer vers le bas
		require
			Valid_stop: can_stop
		do
			going_down := False
			if not going_up then
				sub_image_x := animation_coordinates_vertical.first.x
				sub_image_y := animation_coordinates_vertical.first.y
			end
		ensure
			valid_going: not going_down
		end

	old_timestamp:NATURAL_32
			-- Lors de l'ajout du dernier mouvement (compte tenu de `Movement_delta')

	can_stop:BOOLEAN
		-- le `current' peut arr�ter
feature {NONE} -- Impl�mentation



	movement_delta:NATURAL_32
			-- Le temps de variation entre chaque mouvement de `Current'

	animation_delta:NATURAL_32
			-- Le temps de variation entre chaque animation de `Current'

invariant
	valid_facing:(facing_right or facing_down or facing_left or facing_up) and
				 (facing_right implies not (facing_down or facing_left or facing_up)) and
				 (facing_left implies not (facing_down or facing_right or facing_up)) and
				 (facing_down implies not (facing_right or facing_left or facing_up)) and
				 (facing_up implies not (facing_down or facing_left or facing_right))

	valid_direction:(going_right implies facing_right) or
					(going_left implies facing_left) or
					(going_up implies facing_up) or
					(going_down implies facing_down)

note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
			N'zu� Kouadio Ange Kouassi & Olivier M
			D�partement des techniques de l'informatique
			C�gep de Drummondville
			960, rue Saint-Georges
			Drummondville, (Qu�bec)
			J2C 6A2
			T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859
			Site web: http://www.cegepdrummond.ca
		]"
end
