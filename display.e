note
	description: "Classe abstraite permettant l'affichage des objets"
	auteur: "N'zu� Kouadio Ange Kouassi & Olivier M"
	date: "2020-04-14"
	revision: "1.0"

class
	DISPLAY

create
	make_from_other

feature {NONE}

	make_from_other(a_other:like current)
			-- recup�re les attributs de other et assigne les attributs de `current'
		do
			x := a_other.x
			y := a_other.y
			sub_image_x := a_other.sub_image_x
			sub_image_y := a_other.sub_image_y
			sub_image_width := a_other.sub_image_width
			sub_image_height := a_other.sub_image_height
		end

feature  -- Acc�s

	x:INTEGER assign set_x
			-- Position verticale de `Current'

	y:INTEGER assign set_y
			-- Position horizontale de `Current'

	set_x(a_x:INTEGER)
			-- Attribue la valeur de 'x' � 'a_x'
		require
				X_Positive: a_x >= 0
		do
			x := a_x
		ensure
			Is_Assign: x = a_x
		end

	set_y(a_y:INTEGER)
			-- Attribue la valeur de 'y' � 'a_y'
		require
				Y_Positive: a_y >= 0
		do
			y := a_y
		ensure
			Is_Assign: y = a_y
		end

	sub_image_x, sub_image_y:INTEGER
			-- Position de la partie de l'image � afficher � l'int�rieur de la `surface'

	sub_image_width, sub_image_height:INTEGER
			-- Dimension de la partie de l'image � afficher � l'int�rieur de la `surface'

invariant
note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
			N'zu� Kouadio Ange Kouassi & Olivier M
			D�partement des techniques de l'informatique
			C�gep de Drummondville
			960, rue Saint-Georges
			Drummondville, (Qu�bec)
			J2C 6A2
			T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859
			Site web: http://www.cegepdrummond.ca
		]"
end
