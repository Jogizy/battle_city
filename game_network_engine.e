note
	description: "Le moteur qui g�re la m�canique du serveur."
	author: "N'zu� Kouadio Ange Kouassi & Olivier M"
	date: "2020-05-02"
	revision: "1.0"

class
	GAME_NETWORK_ENGINE

inherit
	GAME_ENGINE
		rename
			make as make_game_engine
		redefine
			hero_control, run, on_iteration
		end

create
	make

feature {NONE} -- Initialisation

	make(a_socket:detachable NETWORK_STREAM_SOCKET; a_window:GAME_WINDOW_RENDERED)
			-- Initialisation de `Current' � utiliser avec `a_socket'
		do
			make_game_engine(a_window)
			if attached a_socket as la_socket then
				create network_server.make(agent hero_fire_network, la_socket)
			end
		end

feature	-- Acc�s

	run
			-- Lance le jeu
		do
			if attached network_server as la_network_server then
				la_network_server.launch
				precursor
				la_network_server.stop
				la_network_server.join
			else
				precursor
			end
		end

	hero_control(a_timestamp:NATURAL_32; a_key_event:GAME_KEY_EVENT)
			-- Contr�le le mouvement de l'h�ro et son tir
		do
			precursor(a_timestamp, a_key_event)
			if a_key_event.is_space and not attached network_server then
				fire(hero, a_timestamp)
			end
		end

feature {NONE} -- Impl�mentation

	network_server:detachable NETWORK_SERVER_GAME
			-- Le r�seau serveur de `Current'

	on_iteration(a_timestamp:NATURAL_32)
			-- �v�nement qui est lanc� � chaque it�ration.
		local
			l_loaders:LIST[DISPLAY]
			l_number:INTEGER
		do
			if hero_has_fire then
				fire(hero, a_timestamp)
				hero_has_fire := False
			end
			precursor(a_timestamp)
			if attached network_server as la_network then
				l_number := enemy_management.enemies.count + map.map_coordinates.count + explosions.count + projectiles.count + 1
				create {ARRAYED_LIST[DISPLAY]}l_loaders.make(l_number)
				l_loaders.extend(create {ANIMATION_LOADER}.make_from_other(hero))
				across enemy_management.enemies as la_enemies  loop
					l_loaders.extend(create {ANIMATION_LOADER}.make_from_other(la_enemies.item))
				end
				across map.map_coordinates as la_coordinates  loop
					l_loaders.extend(create {DISPLAY}.make_from_other(la_coordinates.item))
				end
				across projectiles as la_projectiles  loop
					l_loaders.extend(create {ANIMATION_LOADER}.make_from_other(la_projectiles.item))
				end
				across explosions as la_explosions loop
					l_loaders.extend(create {DISPLAY}.make_from_other(la_explosions.item))
				end
				la_network.send(l_loaders)
			end
		end
feature{NONE}

	hero_fire_network
			-- Cette m�thode met � "True" l'attribut `hero_has_fire'
		do
			hero_has_fire := True
		end

	hero_has_fire:BOOLEAN
			-- Vrai si {HERO} a tir�, sinon faux

invariant
note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
			N'zu� Kouadio Ange Kouassi & Olivier M
			D�partement des techniques de l'informatique
			C�gep de Drummondville
			960, rue Saint-Georges
			Drummondville, (Qu�bec)
			J2C 6A2
			T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859
			Site web: http://www.cegepdrummond.ca
		]"
end
