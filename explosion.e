note
	description: "Explosion anim�e d'une unit� lors de sa destruction"
	auteur: "N'zu� Kouadio Ange Kouassi & Olivier M"
	date: "2020-04-14"
	revision: "1.0"

class
	EXPLOSION

inherit
	DISPLAY

create
	make

feature {NONE} -- Initialisation

	make
			-- Initialisation de `Current'
		do
			sub_image_width := 16
			sub_image_height := 16
			explosion_step := 1
			iteration := 1
			create {ARRAYED_LIST[TUPLE[x,y:INTEGER]]}animation_coordinates_explosion.make(5)
			initialize_animation_coordinate
		end

	initialize_animation_coordinate
			-- Initialisation des coordonn�es d'animation de `Current'
		do
			animation_coordinates_explosion.extend([256,128])
			animation_coordinates_explosion.extend([272,128])
			animation_coordinates_explosion.extend([288,128])
			animation_coordinates_explosion.extend([304,128])
			animation_coordinates_explosion.extend([336,128])
			sub_image_x := animation_coordinates_explosion.first.x
			sub_image_y := animation_coordinates_explosion.first.y
		end

feature -- Acc�s

	update
			-- Met � jour la surface � toutes les 8 it�rations.
		local
			l_coordinate:TUPLE[x,y:INTEGER]
		do
			iteration := iteration + 1
			if iteration >= 8 then
				if explosion_step <= 5 then
					l_coordinate := animation_coordinates_explosion.at(explosion_step)
					explosion_step := explosion_step + 1
					sub_image_x := l_coordinate.x
					sub_image_y := l_coordinate.y
					if explosion_step = 5 then
						x := x - 8
						y := y - 8
						sub_image_width := 32
						sub_image_height := 32
					end
				else
					has_finished := True
				end
				iteration := 1
			end
		end

	has_finished:BOOLEAN
			-- L'�tat de l'explosion de `Current' (vrai si termin�, sinon faux)

feature {NONE} -- Impl�mentation

	animation_coordinates_explosion:LIST[TUPLE[x,y:INTEGER]]
			-- Chaque coordonn�e d'une portion d'images en `surface`

	explosion_step:INTEGER
			-- �tape de l'animation de `Current'

	iteration:INTEGER
			-- Chaque it�ration pour l'animation de `Current'

invariant
	valid_explosion_step: explosion_step >= 1 and explosion_step <= animation_coordinates_explosion.count + 1

note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
			N'zu� Kouadio Ange Kouassi & Olivier M
			D�partement des techniques de l'informatique
			C�gep de Drummondville
			960, rue Saint-Georges
			Drummondville, (Qu�bec)
			J2C 6A2
			T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859
			Site web: http://www.cegepdrummond.ca
		]"
end


