note
	description: "Classe permettant la cr�ation de la carte 1"
	auteur: "Olivier Meloche & N'zu� Kouadio Ange Kouassi"
	date: "2020-05-03"
	revision: "3.0"

class
	MAP_1

inherit
	MAP
		rename
			make as make_map
		end

create
	make

feature {NONE} -- Initialisation

	make(a_tile_factory:TILE_FACTORY)
			-- Initialisation de `Current' � utiliser avec `a_tile_factory'
		do
			make_map
			tile_positioning_1(a_tile_factory)
			tile_positioning_2(a_tile_factory)
			tile_positioning_3(a_tile_factory)
			tile_positioning_4(a_tile_factory)
			tile_positioning_5(a_tile_factory)
			tile_positioning_6(a_tile_factory)
			tile_positioning_7(a_tile_factory)
			tile_positioning_8(a_tile_factory)
			tile_positioning_9(a_tile_factory)
			tile_positioning_10(a_tile_factory)
			tile_positioning_11(a_tile_factory)
			tile_positioning_12(a_tile_factory)
		end

	tile_positioning_1(a_tile_factory:TILE_FACTORY)
			-- Positionnement des tuiles de `Current' � la colonne 1
		local
			l_pixel:INTEGER
		do
			l_pixel := Block_width * 0
			map_coordinates.extend(a_tile_factory.new_steel(l_pixel, Block_width * 6))
		end

	tile_positioning_2(a_tile_factory:TILE_FACTORY)
			-- Positionnement des tuiles de `Current' � la colonne 2
		local
			l_pixel:INTEGER
		do
			l_pixel := Block_width * 1
			map_coordinates.extend(a_tile_factory.new_brick(l_pixel, Block_width))
			map_coordinates.extend(a_tile_factory.new_brick(l_pixel, Block_width * 2))
			map_coordinates.extend(a_tile_factory.new_brick(l_pixel, Block_width * 3))
			map_coordinates.extend(a_tile_factory.new_brick(l_pixel, Block_width * 4))
			map_coordinates.extend(a_tile_factory.new_brick(l_pixel, Block_width * 8))
			map_coordinates.extend(a_tile_factory.new_brick(l_pixel, Block_width * 9))
			map_coordinates.extend(a_tile_factory.new_brick(l_pixel, Block_width * 10))
			map_coordinates.extend(a_tile_factory.new_brick(l_pixel, Block_width * 12))
		end

	tile_positioning_3(a_tile_factory:TILE_FACTORY)
			-- Positionnement des tuiles de `Current' � la colonne 3
		local
			l_pixel:INTEGER
		do
			l_pixel := Block_width * 2
			map_coordinates.extend(a_tile_factory.new_brick(l_pixel, Block_width * 6))
		end

	tile_positioning_4(a_tile_factory:TILE_FACTORY)
			-- Positionnement des tuiles de `Current' � la colonne 4
		local
			l_pixel:INTEGER
		do
			l_pixel := Block_width * 3
			map_coordinates.extend(a_tile_factory.new_brick(l_pixel, Block_width))
			map_coordinates.extend(a_tile_factory.new_brick(l_pixel, Block_width * 2))
			map_coordinates.extend(a_tile_factory.new_brick(l_pixel, Block_width * 3))
			map_coordinates.extend(a_tile_factory.new_brick(l_pixel, Block_width * 4))
			map_coordinates.extend(a_tile_factory.new_brick(l_pixel, Block_width * 6))
			map_coordinates.extend(a_tile_factory.new_brick(l_pixel, Block_width * 8))
			map_coordinates.extend(a_tile_factory.new_brick(l_pixel, Block_width * 10))
			map_coordinates.extend(a_tile_factory.new_brick(l_pixel, Block_width * 11))
			map_coordinates.extend(a_tile_factory.new_brick(l_pixel, Block_width * 12))
		end

	tile_positioning_5(a_tile_factory:TILE_FACTORY)
			-- Positionnement des tuiles de `Current' � la colonne 5
		local
			l_pixel:INTEGER
		do
			l_pixel := Block_width * 4
			map_coordinates.extend(a_tile_factory.new_steel(l_pixel, Block_width * 6))
		end

	tile_positioning_6(a_tile_factory:TILE_FACTORY)
			-- Positionnement des tuiles de `Current' � la colonne 6
		local
			l_pixel:INTEGER
		do
			l_pixel := Block_width * 5
			map_coordinates.extend(a_tile_factory.new_brick(l_pixel, Block_width))
			map_coordinates.extend(a_tile_factory.new_brick(l_pixel, Block_width * 2))
			map_coordinates.extend(a_tile_factory.new_brick(l_pixel, Block_width * 3))
			map_coordinates.extend(a_tile_factory.new_brick(l_pixel, Block_width * 5))
			map_coordinates.extend(a_tile_factory.new_brick(l_pixel, Block_width * 7))
			map_coordinates.extend(a_tile_factory.new_brick(l_pixel, Block_width * 8))
			map_coordinates.extend(a_tile_factory.new_brick(l_pixel, Block_width * 11))
			map_coordinates.extend(a_tile_factory.new_brick(l_pixel, Block_width * 12))
		end

	tile_positioning_7(a_tile_factory:TILE_FACTORY)
			-- Positionnement des tuiles de `Current' � la colonne 7
		local
			l_pixel:INTEGER
		do
			l_pixel := Block_width * 6
			map_coordinates.extend(a_tile_factory.new_steel(l_pixel, Block_width))
			map_coordinates.extend(a_tile_factory.new_brick(l_pixel, Block_width * 11))
			map_coordinates.extend(a_tile_factory.new_king(l_pixel, Block_width * 12)) -- Roi
		end

	tile_positioning_8(a_tile_factory:TILE_FACTORY)
			-- Positionnement des tuiles de `Current' � la colonne 8
		local
			l_pixel:INTEGER
		do
			l_pixel := Block_width * 7
			map_coordinates.extend(a_tile_factory.new_steel(l_pixel, Block_width))
			map_coordinates.extend(a_tile_factory.new_brick(l_pixel, Block_width * 11))
			map_coordinates.extend(a_tile_factory.new_brick(l_pixel, Block_width * 12))
		end

	tile_positioning_9(a_tile_factory:TILE_FACTORY)
			-- Positionnement des tuiles de `Current' � la colonne 9
		local
			l_pixel:INTEGER
		do
			l_pixel := Block_width * 8
			map_coordinates.extend(a_tile_factory.new_steel(l_pixel, Block_width))
			map_coordinates.extend(a_tile_factory.new_grass(l_pixel, Block_width * 2))
			map_coordinates.extend(a_tile_factory.new_steel(l_pixel, Block_width * 3))
			map_coordinates.extend(a_tile_factory.new_grass(l_pixel, Block_width * 4))
			map_coordinates.extend(a_tile_factory.new_steel(l_pixel, Block_width * 5))
			map_coordinates.extend(a_tile_factory.new_grass(l_pixel, Block_width * 6))
			map_coordinates.extend(a_tile_factory.new_steel(l_pixel, Block_width * 8))
			map_coordinates.extend(a_tile_factory.new_grass(l_pixel, Block_width * 9))
			map_coordinates.extend(a_tile_factory.new_grass(l_pixel, Block_width * 10))
			map_coordinates.extend(a_tile_factory.new_steel(l_pixel, Block_width * 11))
		end

	tile_positioning_10(a_tile_factory:TILE_FACTORY)
			-- Positionnement des tuiles de `Current' � la colonne 10
		local
			l_pixel:INTEGER
		do
			l_pixel := Block_width * 9
			map_coordinates.extend(a_tile_factory.new_steel(l_pixel, Block_width))
		end

	tile_positioning_11(a_tile_factory:TILE_FACTORY)
			-- Positionnement des tuiles de `Current' � la colonne 11
		local
			l_pixel:INTEGER
		do
			l_pixel := Block_width * 10
			map_coordinates.extend(a_tile_factory.new_steel(l_pixel, Block_width))
			map_coordinates.extend(a_tile_factory.new_brick(l_pixel, Block_width * 12))
		end

	tile_positioning_12(a_tile_factory:TILE_FACTORY)
			-- Positionnement des tuiles de `Current' � la colonne 12
		local
			l_pixel:INTEGER
		do
			l_pixel := Block_width * 11
			map_coordinates.extend(a_tile_factory.new_brick(l_pixel, Block_width))
			map_coordinates.extend(a_tile_factory.new_brick(l_pixel, Block_width * 3))
			map_coordinates.extend(a_tile_factory.new_brick(l_pixel, Block_width * 8))
			map_coordinates.extend(a_tile_factory.new_brick(l_pixel, Block_width * 9))
			map_coordinates.extend(a_tile_factory.new_brick(l_pixel, Block_width * 10))
		end

	Block_width:INTEGER = 16
			-- La largeur du bloc de tuile

invariant
note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
			N'zu� Kouadio Ange Kouassi & Olivier M
			D�partement des techniques de l'informatique
			C�gep de Drummondville
			960, rue Saint-Georges
			Drummondville, (Qu�bec)
			J2C 6A2
			T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859
			Site web: http://www.cegepdrummond.ca
		]"
end
