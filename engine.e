note
	description: "Moteur commun pour le jeu."
	auteur: "N'zu� Kouadio Ange Kouassi & Olivier M"
	date: "2020-05-02"
	revision: "1.0"

deferred class
	ENGINE

feature {NONE}

	make(a_window:GAME_WINDOW_RENDERED)
			-- Initialisation de `Current'
		do
			window := a_window
			load_tile_sheets
		end

	load_tile_sheets
			-- Chargement de la feuille de tuile contenant les sous-images
		local
			l_pixel_format:GAME_PIXEL_FORMAT
			l_image:IMG_IMAGE_FILE
		do
			create l_image.make("sheet.png")
			if l_image.is_openable then
				l_image.open
				if l_image.is_open then
					create texture.make_from_image(window.renderer, l_image)
				else
					create l_pixel_format
					create texture.make(window.renderer, l_pixel_format, 1, 1)
					has_error := True
				end
			else
				create l_pixel_format
				create texture.make(window.renderer, l_pixel_format, 1, 1)
				has_error := True
			end
		end

feature -- Acc�s

	run
			-- Lance le jeu
		require
			No_Error: not has_error
		deferred
		end

	draw_displayable(a_displayable:DISPLAY)
			-- Dessine un objet `a_displayable' � l'�cran
		do
			if attached{ANIMATION_LOADER}a_displayable as la_display then
				window.renderer.draw_sub_texture_with_mirror(
					texture,
					la_display.sub_image_x, la_display.sub_image_y,
					la_display.sub_image_width, la_display.sub_image_height,
					la_display.x, la_display.y,
					la_display.facing_down, la_display.facing_left
					)
			else
				window.renderer.draw_sub_texture(
					texture,
					a_displayable.sub_image_x, a_displayable.sub_image_y,
					a_displayable.sub_image_width, a_displayable.sub_image_height,
					a_displayable.x, a_displayable.y
					)
			end
		end

	draw_displayables(a_displayables:LIST[DISPLAY])
			-- Dessine chaque objets `a_displayables' � l'�cran
		do
			across
				a_displayables
			as
				la_displayable
			loop
				draw_displayable(la_displayable.item)
			end
		end

	has_error:BOOLEAN
			-- "True" si une erreur s'est produite lors de la cr�ation de `Current'

	window:GAME_WINDOW_RENDERED
			-- La fen�tre pour dessiner la sc�ne

	texture:GAME_TEXTURE
			-- La texture � dessiner
			
invariant
note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
			N'zu� Kouadio Ange Kouassi & Olivier M
			D�partement des techniques de l'informatique
			C�gep de Drummondville
			960, rue Saint-Georges
			Drummondville, (Qu�bec)
			J2C 6A2
			T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859
			Site web: http://www.cegepdrummond.ca
		]"
end
