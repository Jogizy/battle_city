note
	description: "Summary description for {ENGINE_TEXT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	ENGINE_TEXT
inherit
	ENGINE
		redefine
			make
		end

feature {NONE} -- Initialisation
	make(a_window:GAME_WINDOW_RENDERED)
			-- Initialisation de `Current'
		do
			precursor(a_window)
			initialize_font
			create {ARRAYED_LIST[TUPLE[x,y:INTEGER;text:STRING_8;colors:GAME_COLOR; font:TEXT_FONT]]}texts.make(1)
			initialize_text
		end

	initialize_font
			-- Initialise la police texte
		deferred
		end
	initialize_text
			-- Initialise les diff�rents textes de `current'
		deferred
		end

feature -- Acc�s

	on_iteration(a_timestamp:NATURAL_32)
			-- �v�nement qui est lanc� � chaque it�ration.
		deferred
		end

	load_font(a_font_file: STRING;a_number:INTEGER):TEXT_FONT
			-- Charge et retourne une police
		do
			create Result.make (a_font_file, a_number)
			if Result.is_openable then
				Result.open
				has_error := not Result.is_open
			else
				has_error := True
			end
		end

	draw_texts
			-- Cette m�thode dessine chaque �l�ment de texte
		local
			l_text_surface:TEXT_SURFACE_BLENDED
			l_texture_text:GAME_TEXTURE
		do
			across
				texts as la_texts
			loop
				if not la_texts.item.text.is_empty then
					create l_text_surface.make(la_texts.item.text, la_texts.item.font, la_texts.item.colors)
					create l_texture_text.make_from_surface(window.renderer, l_text_surface)
					window.renderer.draw_texture(l_texture_text, la_texts.item.x, la_texts.item.y)
				end
			end
		end

	texts:LIST[TUPLE[x,y:INTEGER; text:STRING; colors:GAME_COLOR; font:TEXT_FONT]]
			-- La liste des textes ainsi que leurs coordonn�es `x', `y', leurs couleurs `colors' et leur police`font'

invariant
note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
			N'zu� Kouadio Ange Kouassi & Olivier M
			D�partement des techniques de l'informatique
			C�gep de Drummondville
			960, rue Saint-Georges
			Drummondville, (Qu�bec)
			J2C 6A2
			T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859
			Site web: http://www.cegepdrummond.ca
		]"
end
