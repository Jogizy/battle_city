note
	description: "Classe de la tuile brique."
	auteur: "N'zu� Kouadio Ange Kouassi & Olivier M"
	date: "2020-04-14"
	revision: "1.0"

class
	TILE_BRICK

inherit
	TILE_BREAKABLE
		redefine
			make
		end

create
	make

feature {NONE} -- Initialisation

	make(a_audio_factory:AUDIO_SOURCE_FACTORY)
			-- Initialisation of `Current' � utiliser avec `a_audio_factory'
		do
			precursor(a_audio_factory)
			is_broken := False
		end

	initialize_tile_coordinate
			-- Initialisation des coordonn�es de `Current'
		do
			sub_image_x := 256
			sub_image_y := 0
		end

feature -- Acc�s

	crack_brick
			-- Change l'aspect de la brique de `Current'
		do
			sub_image_x := 336
			sub_image_y := 0
		end

	play_hit_brick
			-- Joue le son de la brique touch� de `Current'
		do
			sound_hit_brick.play(0)
		end

invariant
note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
			N'zu� Kouadio Ange Kouassi & Olivier M
			D�partement des techniques de l'informatique
			C�gep de Drummondville
			960, rue Saint-Georges
			Drummondville, (Qu�bec)
			J2C 6A2
			T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859
			Site web: http://www.cegepdrummond.ca
		]"
end
