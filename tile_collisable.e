note
	description: "Classe abstraite de la tuile collisable."
	auteur: "N'zu� Kouadio Ange Kouassi & Olivier M"
	date: "2020-04-14"
	revision: "1.0"

deferred class
	TILE_COLLISABLE

inherit
	COLLISABLE
		rename
			width as sub_image_width,
			height as sub_image_height
		end
	TILE
		rename
			make as make_tile
		end

feature {NONE} -- Initialisation

	make(a_audio_factory:AUDIO_SOURCE_FACTORY)
			-- Initialisation de `Current' � utiliser avec `a_audio_factory'
		do
			make_tile
			create sound_hit_steel.make(a_audio_factory.source_hit_steel, "snd_hit_steel.wav")
			create sound_hit_brick.make(a_audio_factory.source_hit_brick, "snd_hit_brick.wav")
			create sound_hit_king.make(a_audio_factory.source_hit_king, "snd_hit_king.wav")
			create sound_hit_water.make(a_audio_factory.source_hit_water, "snd_hit_water.wav")
		end

feature {NONE} -- Impl�mentation

	sound_hit_steel:AUDIO
			-- Le son de l'acier touch�

	sound_hit_brick:AUDIO
			-- Le son de la brique touch�e

	sound_hit_king:AUDIO
			-- Le son du roi touch�

	sound_hit_water:AUDIO
			-- Le son de l'eau touch�e

invariant
note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
			N'zu� Kouadio Ange Kouassi & Olivier M
			D�partement des techniques de l'informatique
			C�gep de Drummondville
			960, rue Saint-Georges
			Drummondville, (Qu�bec)
			J2C 6A2
			T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859
			Site web: http://www.cegepdrummond.ca
		]"
end
