note
	description: "Classe abstraite permettant la cr�ation d'unit� et de modifier son �tat."
	auteur: "N'zu� Kouadio Ange Kouassi & Olivier M"
	date: "2020-04-14"
	revision: "4.0"

deferred class
	UNIT

inherit
	COLLISABLE
		rename
			width as sub_image_width,
			height as sub_image_height
		end
	ANIMATION
		rename
			make as make_animation
		redefine
			update
		end

feature {NONE} -- Initialisation

	make(a_audio_factory:AUDIO_SOURCE_FACTORY)
			-- Initialisation de `Current' � utiliser avec `a_audio_factory'
		do
			make_animation
			create sound_move.make(a_audio_factory.source_move, "snd_move.wav")
			create sound_fire.make(a_audio_factory.source_fire, "snd_fire.wav")
			create sound_explosion.make(a_audio_factory.source_explosion, "snd_explosion.wav")
			create sound_hit_unit.make(a_audio_factory.source_hit_unit, "snd_hit_unit.wav")
			sub_image_width := 15
			sub_image_height := 15
			initialize_hitpoints
		end

	initialize_hitpoints
			-- Initialisation du nombre de points de vies de `Current'
		deferred
		end

	sound_move:AUDIO
			-- Le son de d�placement de `Current'

	sound_fire:AUDIO
			-- Le son de tir de `Current'

	sound_explosion:AUDIO
			-- Le son d'explosion de `Current'

	sound_hit_unit:AUDIO
			-- Le son de `Current' touch�

feature -- Acc�s

	play_fire
			-- Joue le son de tir de `Current'
		do
			sound_fire.play(0)
		end

	play_explode
			-- Joue le son d'explosion de `Current'
		do
			sound_explosion.play(0)
		end

	stop_moving
			-- Met tous les �tat de d�placement de `Current' � Faux
		do
			going_up := False
			going_down := False
			going_left := False
			going_right := False
		end

	is_moving
			-- Joue le son de d�placement de `Current'
		do
			if not (going_up or going_down or going_left or going_right) then
				sound_move.stop
			else
				sound_move.play(-1)
			end
		end

	new_projectile(a_timestamp:NATURAL_32)
			-- Cr�ation d'un nouveau projectile en fonction de `a_timestamp' de `Current'
		require
			No_shooting: not is_shooting
		local
			l_projectile:PROJECTILE
		do
			create l_projectile.make
			l_projectile.set_x(x + 6)
			l_projectile.set_y(y + 6)
			if facing_right then
				l_projectile.go_right(a_timestamp)
			elseif facing_left then
				l_projectile.go_left(a_timestamp)
			elseif facing_up then
				l_projectile.go_up(a_timestamp)
			elseif facing_down then
				l_projectile.go_down(a_timestamp)
			end
			projectile := l_projectile
		end

	projectile:detachable PROJECTILE
			-- Le projectile detachable de `Current'

	new_explosion
			-- Cr�ation d'une nouvelle explosion de `Current'
		local
			l_explosion:EXPLOSION
		do
			create l_explosion.make
			l_explosion.set_x(x)
			l_explosion.set_y(y)
			explosion := l_explosion
		end

	explosion:detachable EXPLOSION
			-- L'explosion detachable de `Current'

	damage
			-- Diminution du nombre de points de vies de `Current'
		do
			sound_hit_unit.play(0)
			hitpoints := hitpoints - 1
		end

	update(a_timestamp:NATURAL_32)
			-- <Precursor>
		do
			Precursor(a_timestamp)
			if attached projectile as la_projectile and then la_projectile.is_destroyed then
				projectile := Void
			end
		end

	is_shooting:BOOLEAN
			-- L'�tat de tir de `Current' (vrai si est en train de tirer, sinon faux)
		do
			Result := attached projectile
		end

	hitpoints:INTEGER
			-- Les points de vies de `Current'

invariant
	valid_hit_points: hitpoints >= 0
	valid_shooting: is_shooting implies attached projectile

note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
			N'zu� Kouadio Ange Kouassi & Olivier M
			D�partement des techniques de l'informatique
			C�gep de Drummondville
			960, rue Saint-Georges
			Drummondville, (Qu�bec)
			J2C 6A2
			T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859
			Site web: http://www.cegepdrummond.ca
		]"
end
