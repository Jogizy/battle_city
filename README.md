#######################################################################
# Projet jeu: Battle-City
# Auteur: Kouassi N'zué kouadio ange & Olivier Meloche
# Date: 2020-06-07
# Version: 1.0
#######################################################################

----------------------------------------------------
INSTRUCTIONS DE COMPILATION ET D'EXECUTION DU PROJET
----------------------------------------------------

***
 1- Guide d’installation
 =======================
 
    Sur Windows

	 1.Installation d'EiffelStudio :

	    • Télécharger EiffelStudio à l'adresse: https://www.eiffel.org/
	    • Installer EiffelStudio

	 2.Installation d'Eiffel Game 2 :

	    • Télécharger Eiffel_Game2.zip : https://github.com/tioui/Eiffel_Game2/tree/windows_build
            • Copier le répertoire "game2" dans le sous-répertoire d'EiffelStudio contrib/library/
	    • Copier les fichiers dll du fichier zip correspondant à votre version d'Eiffelstudio 
	      (DLL32.zip ou DLL64.zip) dans le répertoire de votre projet (le même répertoire que votre fichier .ecf).

     Sur Linux

	 1.Installez EiffelStudio
	
             • sudo add-apt-repository ppa:eiffelstudio-team/ppa
             • sudo apt-get update
             • sudo apt-get install eiffelstudio

         2.Installez les librairies C :

             • sudo apt install git libopenal-dev libsndfile1-dev libgles2-mesa-dev
             • sudo apt install libsdl2-dev libsdl2-gfx-dev libsdl2-image-dev libsdl2-ttf-dev
             • sudo apt install libepoxy-dev libgl1-mesa-dev libglu1-mesa-dev

         3.Télécharger Eiffel Game 2

             •git clone --recursive https://github.com/tioui/Eiffel_Game2.git

         4.Créer un lien entre le répertoire EiffelStudio et Eiffel_Game2 :

             •sudo ln -s `pwd`/Eiffel_Game2 /usr/lib/eiffelstudio-18.11/contrib/library/game2
              Si cette commande vous donne une erreur de type chemin introuvable, vérifiez la
              version d’EiffelStudio installé sur votre système avec la commande suivant et adaptez
              la commande précédente en changeant le numéro de version.
             •ls /usr/lib/ | grep eiffelstudio

         5.Compiler la librairie :

            •cd Eiffel_Game2
            •./compile_c_library.sh
***
--------------------------------
COMMENT JOUER AU JEU BATTLE-CITY
--------------------------------

  Guide d’utilisation
  

    1. Partie solo « single »

          - Ouvrir le dossier battle_city
          - Ouvrir le fichier battle_city.ecf
          - Choisir le Target « single »
          - Compiler le programme de jeu 
          - Lancer le jeu

        Le jeu battle_city lancé, affichera un menu.
        Le menu a deux options :
          - Option « START » : Appuyez sur la touche « Enter » ou sur la touche numérique « 1 » pour commencer le jeu.
          - Option « QUIT » : Appuyez sur la touche « esc » pour quitter le jeu.
        Choisir l’Option « START » pour commencer le jeu. Une fenêtre s’ouvrira avec le texte STAGE 1.
 
        Ensuite, appuyez sur la touche « c » pour débuter la première partie (STAGE 1).
          - Si le STAGE 1 est remporté, une autre fenêtre s’affichera pour la deuxième partie (STAGE 2).
          - Appuyez sur la touche « c » pour débuter la deuxième partie (STAGE 2)
          - Si le STAGE 2 est remporté, une autre fenêtre s’affichera pour la troisième partie (STAGE 3).
          - Appuyez sur la touche « c » pour débuter la troisième partie (STAGE 3).
 
        Si le STAGE 3 est remporté, cette fenêtre s’affichera avec le texte "YOU WIN !".				 
        Pour revenir au menu du jeu quand la partie est gagnée "YOU WIN !",  appuyez sur la touche « c ».

        En cas d’échec, cette autre fenêtre s’affichera avec le texte "GAME OVER" .				 
        Pour revenir au menu  du jeu quand la partie est terminée "GAME OVER", appuyez  sur la touche «  c ».

       Principe du jeu
       ===============

        Le joueur dirige le véhicule jaune à partir des touches directionnelles du clavier contre les véhicules
        ennemis de couleur grise. Il tire avec la touche « Espace » contre les ennemis. Il doit protéger le roi
        « King » entouré de briques contre les véhicules ennemis et aussi, il doit éviter de se faire tirer là-dessus.
        Après sept ennemis de tués, la partie encours (STAGE…) se termine et une autre débute. Le véhicule jaune a
        trois vies. À chaque fois qu’il se fait tirer, il perd une vie jusqu'à ce qu’il n’en possède plus et donc,
        c’est la fin de la partie "GAME OVER". Si le roi « King » qui doit être protégé, se fait tirer par les ennemis
        ou le véhicule jaune lui- même, c’est encore la fin de la partie "GAME OVER".

        Les Touches
        ===========
          - Touche « Enter » ou « 1 » : Commencer le jeu
          - Touche « esc » : Quitter le jeu
          - Touche « c » : Commencer le stage ou revenir au menu après une victoire finale (YOU WIN !) ou une partie
            terminée (GAME OVER)
          - Touches directionnelles : Déplacer le véhicule jaune en fonction des touches (haut, bas, gauche, droite)
          - Touche « Espace » : tir du véhicule jaune
          - Touche « - » : Baisse le volume de la musique du jeu
          - Touche « = » : Augmente  le volume de la musique du jeu
          - Touche « m » : Arrête ou relance la musique du jeu

  2. Partie réseau (client-serveur)

     Cette partie permet d’effectuer  des tirs du véhicule jaune via le réseau. L’ouverture du fichier
     battle_city.ecf sera réalisé par deux fois : une pour la partie client et une autre pour la partie serveur.

     Pour cela, il faut:
       - Ouvrir le dossier battle_city
       - Ouvrir le fichier battle_city.ecf
       - Choisir le Target « server »
       - Compiler le programme de jeu 
       - Lancer le jeu
       - Ouvrir  une seconde fois le fichier battle_city.ecf
       - Choisir le Target « customer »
       - Compiler le programme de jeu 
       - Lancer le jeu
    Deux fenêtres apparaitront, une pour la partie serveur et l’autre pour la partie Client pour permettre
    de jouer via le réseau.

***

License
-------

    Copyright (c) 2020 Kouassi N'zué Kouadio ANGE & Olivier Meloche

    L'autorisation est accordée, gracieusement, à toute personne acquérant une
    copie de cette bibliothèque et des fichiers de documentation associés
    (la "Bibliothèque"), de commercialiser la Bibliothèque sans restriction,
    notamment les droits d'utiliser, de copier, de modifier, de fusionner, de
    publier, de distribuer, de sous-licencier et / ou de vendre des copies de
    la Bibliothèque, ainsi que d'autoriser les personnes auxquelles la
    Bibliothèque est fournie à le faire, sous réserve des conditions suivantes:

    La déclaration de copyright ci-dessus et la présente autorisation doivent
    être incluses dans toutes copies ou parties substantielles de la
    Bibliothèque.

    LA BIBLIOTHÈQUE EST FOURNIE "TELLE QUELLE", SANS GARANTIE D'AUCUNE SORTE,
    EXPLICITE OU IMPLICITE, NOTAMMENT SANS GARANTIE DE QUALITÉ MARCHANDE,
    D’ADÉQUATION À UN USAGE PARTICULIER ET D'ABSENCE DE CONTREFAÇON. EN AUCUN
    CAS, LES AUTEURS OU TITULAIRES DU DROIT D'AUTEUR NE SERONT RESPONSABLES DE
    TOUT DOMMAGE, RÉCLAMATION OU AUTRE RESPONSABILITÉ, QUE CE SOIT DANS LE
    CADRE D'UN CONTRAT, D'UN DÉLIT OU AUTRE, EN PROVENANCE DE, CONSÉCUTIF À OU
    EN RELATION AVEC LA BIBLIOTHÈQUE OU SON UTILISATION, OU AVEC D'AUTRES
    ÉLÉMENTS DE LA BIBLIOTHÈQUE.