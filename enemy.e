note
	description: "Classe abstraite des actions/d�placements possibles d'un ennemi."
	auteur: "N'zu� Kouadio Ange Kouassi & Olivier M"
	date: "2020-04-14"
	revision: "1.0"

deferred class
	ENEMY

inherit
	GAME_RANDOM_SHARED
	UNIT
		rename
			make as make_unit
		redefine
			go_up, go_down, go_left, go_right
		end

feature {NONE} -- Initialisation

	make(a_audio_factory:AUDIO_SOURCE_FACTORY; a_timestamp:NATURAL_32)
			-- Initialisation de `Current' � utiliser avec `a_audio_factory'
		do
			make_unit(a_audio_factory)
			has_collided := True
			new_direction(a_timestamp)
			can_stop := False
		end

feature -- Acc�s

	new_direction(a_timestamp:NATURAL_32)
			-- Assigne une nouvelle direction � `Current'
		local
			l_integer:INTEGER
		do
			if has_collided then
				random.generate_new_random
				l_integer := random.last_random_integer_between(1, 4)
				if l_integer = 1 then
					go_down(a_timestamp)
				elseif l_integer = 2 then
					go_up(a_timestamp)
				elseif l_integer = 3 then
					go_left(a_timestamp)
				elseif l_integer = 4 then
					go_right(a_timestamp)
				end
				has_collided := False
			end
		end

	go_left(a_timestamp:NATURAL_32)
			-- Faire `Current' commencer � se d�placer vers la gauche
		do
			precursor(a_timestamp)
			going_right := False
			going_down := False
			going_up := False
		end

	go_right(a_timestamp:NATURAL_32)
			-- Faire `Current' commencer � se d�placer vers la droite
		do
			precursor(a_timestamp)
			going_left := False
			going_down := False
			going_up := False
		end

	go_up(a_timestamp:NATURAL_32)
			-- Faire `Current' commencer � se d�placer vers le haut
		do
			precursor(a_timestamp)
			going_right := False
			going_down := False
			going_left := False
		end

	go_down(a_timestamp:NATURAL_32)
			-- Faire `Current' commencer � se d�placer vers le bas
		do
			precursor(a_timestamp)
			going_right := False
			going_left := False
			going_up := False
		end

	has_collided:BOOLEAN assign set_has_collided
			-- Vrai si est entrain de faire une collision avec un obstacle, sinon faux

	set_has_collided(a_collided:BOOLEAN)
			-- Attribue la valeur de 'has_collided' � 'a_collided'
		do
			has_collided := a_collided
		ensure
			Is_Assign: has_collided = a_collided
		end

invariant
	valid_going: going_right or going_down or going_left or going_up

note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
			N'zu� Kouadio Ange Kouassi & Olivier M
			D�partement des techniques de l'informatique
			C�gep de Drummondville
			960, rue Saint-Georges
			Drummondville, (Qu�bec)
			J2C 6A2
			T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859
			Site web: http://www.cegepdrummond.ca
		]"
end
