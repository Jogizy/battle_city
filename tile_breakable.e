note
	description: "Classe abstraite permettant la brisure de tuile."
	auteur: "N'zu� Kouadio Ange Kouassi & Olivier M"
	date: "2020-04-14"
	revision: "1.0"

deferred class
	TILE_BREAKABLE

inherit
	TILE_COLLISABLE

feature -- Acc�s

	is_broken :BOOLEAN assign set_broken
			-- L'�tat de `Current'

	set_broken(a_is_cracked:BOOLEAN)
			-- Attribue la valeur de 'is_broken' � 'is_cracked'
		do
			is_broken := a_is_cracked
		ensure
			Is_Assign: is_broken = a_is_cracked
		end

invariant
note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
			N'zu� Kouadio Ange Kouassi & Olivier M
			D�partement des techniques de l'informatique
			C�gep de Drummondville
			960, rue Saint-Georges
			Drummondville, (Qu�bec)
			J2C 6A2
			T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859
			Site web: http://www.cegepdrummond.ca
		]"
end
