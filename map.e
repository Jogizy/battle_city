note
	description: "Classe abstraite permettant de cr�er une carte de jeu"
	author: "Olivier Meloche & N'zu� Kouadio Ange Kouassi"
	date: "2020-02-24"
	revision: "1.0"

deferred class
	MAP

inherit
	DISPLAY

feature {NONE} -- Initialisation

	make
			-- Initialisation de `Current'
		do
			create {ARRAYED_LIST[TILE]} map_coordinates.make(1)
			sub_image_width := 16
			sub_image_height := 16
		end

feature -- Acc�s

	map_coordinates:LIST[TILE]
			-- La liste des tuiles de `Current'

invariant
note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
			N'zu� Kouadio Ange Kouassi & Olivier M
			D�partement des techniques de l'informatique
			C�gep de Drummondville
			960, rue Saint-Georges
			Drummondville, (Qu�bec)
			J2C 6A2
			T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859
			Site web: http://www.cegepdrummond.ca
		]"
end
