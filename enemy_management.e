note
	description: "Classe permettant la gestion des ennemis"
	auteur: "N'zu� Kouadio Ange Kouassi & Olivier M"
	date: "2020-05-04"
	revision: "2.0"

class
	ENEMY_MANAGEMENT

inherit
	GAME_RANDOM_SHARED

create
	make

feature {NONE} -- Initialisation

	make(a_enemy_factory:ENEMY_FACTORY)
			-- Initialisation de `Current' � utiliser avec `a_enemy_factory'
		do
			enemy_factory := a_enemy_factory
			create {ARRAYED_LIST[ENEMY]}enemies.make(3)
		end

feature -- Acc�s

	new_enemy(a_x, a_y:INTEGER; a_timestamp:NATURAL_32)
			-- S�lection d'un nouvel ennemi cr�� en fonction
			-- de ses coordonn�es `a_x' et `a_y'
		local
			l_integer:INTEGER
		do
			random.generate_new_random
			l_integer := random.last_random_integer_between(1, 3)
			if l_integer = 1 then
				enemies.extend(enemy_factory.new_char(a_x, a_y,a_timestamp))
			elseif l_integer = 2 then
				enemies.extend(enemy_factory.new_panzer(a_x, a_y, a_timestamp))
			elseif l_integer = 3 then
				enemies.extend(enemy_factory.new_tank(a_x, a_y, a_timestamp))
			end
		end

	enemies:LIST[ENEMY]
			-- La liste des ennemis de `Current'

feature {NONE} -- Impl�mentation

	enemy_factory:ENEMY_FACTORY
			-- Le g�n�rateur d'ennemis de `Current'
invariant
note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
			N'zu� Kouadio Ange Kouassi & Olivier M
			D�partement des techniques de l'informatique
			C�gep de Drummondville
			960, rue Saint-Georges
			Drummondville, (Qu�bec)
			J2C 6A2
			T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859
			Site web: http://www.cegepdrummond.ca
		]"
end
