note
	description: "Classe permettant la cr�ation de la carte 2"
	auteur: "Olivier Meloche & N'zu� Kouadio Ange Kouassi"
	date: "2020-05-03"
	revision: "3.0"

class
	MAP_2

inherit
	MAP
		rename
			make as make_map
		end

create
	make

feature {NONE} -- Initialisation

	make(a_tile_factory:TILE_FACTORY)
			-- Initialisation de `Current' � utiliser avec `a_tile_factory'
		do
			make_map
			tile_positioning_1(a_tile_factory)
			tile_positioning_2(a_tile_factory)
			tile_positioning_3(a_tile_factory)
			tile_positioning_4(a_tile_factory)
			tile_positioning_5(a_tile_factory)
			tile_positioning_6(a_tile_factory)
			tile_positioning_7(a_tile_factory)
			tile_positioning_8(a_tile_factory)
			tile_positioning_9(a_tile_factory)
			tile_positioning_10(a_tile_factory)
			tile_positioning_11(a_tile_factory)
			tile_positioning_12(a_tile_factory)
			tile_positioning_13(a_tile_factory)
		end

	tile_positioning_1(a_tile_factory:TILE_FACTORY)
			-- Positionnement des tuiles de `Current' � la ligne 1
		local
			l_pixel:INTEGER
		do
			l_pixel := Block_width * 0
			map_coordinates.extend(a_tile_factory.new_brick(Block_width * 2, l_pixel))
			map_coordinates.extend(a_tile_factory.new_brick(Block_width * 7, l_pixel))
			map_coordinates.extend(a_tile_factory.new_brick(Block_width * 9, l_pixel))
		end

	tile_positioning_2(a_tile_factory:TILE_FACTORY)
			-- Positionnement des tuiles de `Current' � la ligne 2
		local
			l_pixel:INTEGER
		do
			l_pixel := Block_width * 1
			map_coordinates.extend(a_tile_factory.new_grass(Block_width * 0, l_pixel))
			map_coordinates.extend(a_tile_factory.new_brick(Block_width, l_pixel))
			map_coordinates.extend(a_tile_factory.new_brick(Block_width * 2, l_pixel))
			map_coordinates.extend(a_tile_factory.new_brick(Block_width * 3, l_pixel))
			map_coordinates.extend(a_tile_factory.new_brick(Block_width * 5, l_pixel))
			map_coordinates.extend(a_tile_factory.new_brick(Block_width * 7, l_pixel))
			map_coordinates.extend(a_tile_factory.new_brick(Block_width * 9, l_pixel))
		end

	tile_positioning_3(a_tile_factory:TILE_FACTORY)
			-- Positionnement des tuiles de `Current' � la ligne 3
		local
			l_pixel:INTEGER
		do
			l_pixel := Block_width * 2
			map_coordinates.extend(a_tile_factory.new_grass(Block_width * 0, l_pixel))
			map_coordinates.extend(a_tile_factory.new_grass(Block_width, l_pixel))
			map_coordinates.extend(a_tile_factory.new_grass(Block_width * 2, l_pixel))
			map_coordinates.extend(a_tile_factory.new_grass(Block_width * 7, l_pixel))
			map_coordinates.extend(a_tile_factory.new_grass(Block_width * 8, l_pixel))
			map_coordinates.extend(a_tile_factory.new_grass(Block_width * 9, l_pixel))
			map_coordinates.extend(a_tile_factory.new_grass(Block_width * 11, l_pixel))
			map_coordinates.extend(a_tile_factory.new_grass(Block_width * 12, l_pixel))
		end

	tile_positioning_4(a_tile_factory:TILE_FACTORY)
			-- Positionnement des tuiles de `Current' � la ligne 4
		local
			l_pixel:INTEGER
		do
			l_pixel := Block_width * 3
			map_coordinates.extend(a_tile_factory.new_grass(Block_width * 0, l_pixel))
			map_coordinates.extend(a_tile_factory.new_water(Block_width, l_pixel))
			map_coordinates.extend(a_tile_factory.new_water(Block_width * 2, l_pixel))
			map_coordinates.extend(a_tile_factory.new_grass(Block_width * 3, l_pixel))
			map_coordinates.extend(a_tile_factory.new_water(Block_width * 5, l_pixel))
			map_coordinates.extend(a_tile_factory.new_water(Block_width * 6, l_pixel))
			map_coordinates.extend(a_tile_factory.new_water(Block_width * 8, l_pixel))
			map_coordinates.extend(a_tile_factory.new_water(Block_width * 9, l_pixel))
			map_coordinates.extend(a_tile_factory.new_water(Block_width * 10, l_pixel))
			map_coordinates.extend(a_tile_factory.new_water(Block_width * 11, l_pixel))
		end

	tile_positioning_5(a_tile_factory:TILE_FACTORY)
			-- Positionnement des tuiles de `Current' � la ligne 5
		local
			l_pixel:INTEGER
		do
			l_pixel := Block_width * 4
			map_coordinates.extend(a_tile_factory.new_brick(Block_width, l_pixel))
			map_coordinates.extend(a_tile_factory.new_grass(Block_width * 9, l_pixel))
			map_coordinates.extend(a_tile_factory.new_grass(Block_width * 10, l_pixel))
			map_coordinates.extend(a_tile_factory.new_grass(Block_width * 11, l_pixel))
			map_coordinates.extend(a_tile_factory.new_grass(Block_width * 12, l_pixel))
		end

	tile_positioning_6(a_tile_factory:TILE_FACTORY)
			-- Positionnement des objets de `Current' � la ligne 6
		local
			l_pixel:INTEGER
		do
			l_pixel := Block_width * 5
			map_coordinates.extend(a_tile_factory.new_brick(Block_width * 2, l_pixel))
			map_coordinates.extend(a_tile_factory.new_brick(Block_width * 5, l_pixel))
			map_coordinates.extend(a_tile_factory.new_brick(Block_width * 6, l_pixel))
			map_coordinates.extend(a_tile_factory.new_grass(Block_width * 7, l_pixel))
			map_coordinates.extend(a_tile_factory.new_brick(Block_width * 8, l_pixel))
			map_coordinates.extend(a_tile_factory.new_brick(Block_width * 9, l_pixel))
		end

	tile_positioning_7(a_tile_factory:TILE_FACTORY)
			-- Positionnement des tuiles de `Current' � la ligne 7
		local
			l_pixel:INTEGER
		do
			l_pixel := Block_width * 6
			map_coordinates.extend(a_tile_factory.new_brick(Block_width * 0, l_pixel))
			map_coordinates.extend(a_tile_factory.new_brick(Block_width, l_pixel))
			map_coordinates.extend(a_tile_factory.new_brick(Block_width * 2, l_pixel))
			map_coordinates.extend(a_tile_factory.new_grass(Block_width * 4, l_pixel))
			map_coordinates.extend(a_tile_factory.new_grass(Block_width * 5, l_pixel))
			map_coordinates.extend(a_tile_factory.new_grass(Block_width * 6, l_pixel))
			map_coordinates.extend(a_tile_factory.new_brick(Block_width * 7, l_pixel))
			map_coordinates.extend(a_tile_factory.new_brick(Block_width * 8, l_pixel))
			map_coordinates.extend(a_tile_factory.new_grass(Block_width * 9, l_pixel))
			map_coordinates.extend(a_tile_factory.new_brick(Block_width * 10, l_pixel))
			map_coordinates.extend(a_tile_factory.new_steel(Block_width * 11, l_pixel))
		end

	tile_positioning_8(a_tile_factory:TILE_FACTORY)
			-- Positionnement des tuiles de `Current' � la ligne 8
		local
			l_pixel:INTEGER
		do
			l_pixel := Block_width * 7
			map_coordinates.extend(a_tile_factory.new_steel(Block_width * 3, l_pixel))
			map_coordinates.extend(a_tile_factory.new_brick(Block_width * 5, l_pixel))
			map_coordinates.extend(a_tile_factory.new_grass(Block_width * 6, l_pixel))
			map_coordinates.extend(a_tile_factory.new_grass(Block_width * 7, l_pixel))
			map_coordinates.extend(a_tile_factory.new_grass(Block_width * 8, l_pixel))
			map_coordinates.extend(a_tile_factory.new_grass(Block_width * 9, l_pixel))
			map_coordinates.extend(a_tile_factory.new_grass(Block_width * 10, l_pixel))
		end

	tile_positioning_9(a_tile_factory:TILE_FACTORY)
			-- Positionnement des tuiles de `Current' � la ligne 9
		local
			l_pixel:INTEGER
		do
			l_pixel := Block_width * 8
			map_coordinates.extend(a_tile_factory.new_water(Block_width, l_pixel))
			map_coordinates.extend(a_tile_factory.new_water(Block_width * 3, l_pixel))
			map_coordinates.extend(a_tile_factory.new_water(Block_width * 4, l_pixel))
			map_coordinates.extend(a_tile_factory.new_water(Block_width * 5, l_pixel))
			map_coordinates.extend(a_tile_factory.new_water(Block_width * 6, l_pixel))
			map_coordinates.extend(a_tile_factory.new_water(Block_width * 7, l_pixel))
			map_coordinates.extend(a_tile_factory.new_water(Block_width * 10, l_pixel))
			map_coordinates.extend(a_tile_factory.new_water(Block_width * 11, l_pixel))
		end

	tile_positioning_10(a_tile_factory:TILE_FACTORY)
			-- Positionnement des tuiles de `Current' � la ligne 10
		local
			l_pixel:INTEGER
		do
			l_pixel := Block_width * 9
			map_coordinates.extend(a_tile_factory.new_grass(Block_width * 0, l_pixel))
			map_coordinates.extend(a_tile_factory.new_grass(Block_width, l_pixel))
			map_coordinates.extend(a_tile_factory.new_brick(Block_width * 5, l_pixel))
		end

	tile_positioning_11(a_tile_factory:TILE_FACTORY)
			-- Positionnement des objets de `Current' � la ligne 11
		local
			l_pixel:INTEGER
		do
			l_pixel := Block_width * 10
			map_coordinates.extend(a_tile_factory.new_grass(Block_width * 0, l_pixel))
			map_coordinates.extend(a_tile_factory.new_grass(Block_width, l_pixel))
			map_coordinates.extend(a_tile_factory.new_brick(Block_width * 2, l_pixel))
			map_coordinates.extend(a_tile_factory.new_brick(Block_width * 9, l_pixel))
			map_coordinates.extend(a_tile_factory.new_brick(Block_width * 10, l_pixel))
			map_coordinates.extend(a_tile_factory.new_steel(Block_width * 12, l_pixel))
		end

	tile_positioning_12(a_tile_factory:TILE_FACTORY)
			-- Positionnement des tuiles de `Current' � la ligne 12
		local
			l_pixel:INTEGER
		do
			l_pixel := Block_width * 11
			map_coordinates.extend(a_tile_factory.new_steel(Block_width, l_pixel))
			map_coordinates.extend(a_tile_factory.new_brick(Block_width * 2, l_pixel))
			map_coordinates.extend(a_tile_factory.new_brick(Block_width * 3, l_pixel))
			map_coordinates.extend(a_tile_factory.new_brick(Block_width * 5, l_pixel))
			map_coordinates.extend(a_tile_factory.new_brick(Block_width * 6, l_pixel))
			map_coordinates.extend(a_tile_factory.new_brick(Block_width * 7, l_pixel))
		end

	tile_positioning_13(a_tile_factory:TILE_FACTORY)
			-- Positionnement des tuiles de `Current' � la ligne 13
		local
			l_pixel:INTEGER
		do
			l_pixel := Block_width * 12
			map_coordinates.extend(a_tile_factory.new_brick(Block_width * 2, l_pixel))
			map_coordinates.extend(a_tile_factory.new_brick(Block_width * 3, l_pixel))
			map_coordinates.extend(a_tile_factory.new_brick(Block_width * 5, l_pixel))
			map_coordinates.extend(a_tile_factory.new_king (Block_width * 6, l_pixel)) -- Roi
			map_coordinates.extend(a_tile_factory.new_brick(Block_width * 7, l_pixel))
			map_coordinates.extend(a_tile_factory.new_brick(Block_width * 10, l_pixel))
		end

	Block_width:INTEGER = 16
			-- La largeur du bloc de tuile


note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
			N'zu� Kouadio Ange Kouassi & Olivier M
			D�partement des techniques de l'informatique
			C�gep de Drummondville
			960, rue Saint-Georges
			Drummondville, (Qu�bec)
			J2C 6A2
			T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859
			Site web: http://www.cegepdrummond.ca
		]"
end
