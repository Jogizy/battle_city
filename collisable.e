note
	description: "Classe abstraite permettant de d�tecter la collision entre les objets"
	auteur: "N'zu� Kouadio Ange Kouassi & Olivier M"
	date: "2020-04-09"
	revision: "1.0"

deferred class
	COLLISABLE

feature -- Acc�s

	has_collision(a_collisable_object:COLLISABLE):BOOLEAN
			-- Collision d'un objet `a_collisable_object' avec un autre objet
			-- Retourne vrai si collision, sinon faux
		local
			l_result:BOOLEAN
		do
			l_result := False
			if x + width > a_collisable_object.x then
				if x < a_collisable_object.x + a_collisable_object.width then
					if y < a_collisable_object.y + a_collisable_object.height then
						if y + height > a_collisable_object.y then
							l_result := True
						end
					end
				end
			end
			Result := l_result
		end

	x, y:INTEGER
			-- Position verticale et horizontal de `Current'
		deferred
		end

	width, height:INTEGER
			-- Dimension de la partie de l'image � afficher � l'int�rieur de la `surface'
		deferred
		end

invariant
note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
			N'zu� Kouadio Ange Kouassi & Olivier M
			D�partement des techniques de l'informatique
			C�gep de Drummondville
			960, rue Saint-Georges
			Drummondville, (Qu�bec)
			J2C 6A2
			T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859
			Site web: http://www.cegepdrummond.ca
		]"
end
