note
	description: "Classe permettant de d�truire les objets collisables."
	auteur: "N'zu� Kouadio Ange Kouassi & Olivier M"
	date: "2020-04-14"
	revision: "2.0"

class
	PROJECTILE

inherit
	COLLISABLE
		rename
			width as sub_image_width,
			height as sub_image_height
		end
	ANIMATION
		redefine
			make
		end

create
	make

feature {NONE} -- Initialisation

	make
			-- Initialisation de `Current'
		do
			precursor
			initialize_animation_coordinate
			initialize_speed
			sub_image_width := 4
			sub_image_height := 4
			is_destroyed := False
		end

	initialize_animation_coordinate
			-- Initialisation des coordonn�es d'animation de `Current'
		do
			animation_coordinates_horizontal.extend([342,103])
			animation_coordinates_vertical.extend([327,86])
		end

	initialize_speed
			-- Initialisation de la vitesse de d�placement de `Current'
		do
			movement_delta := 10
			animation_delta := 100
		end

feature -- Acc�s

	is_destroyed:BOOLEAN
			-- L'�tat du projectile (vrai si d�truit, sinon faux)

	destroy_bullet
			-- Change l'�tat du projectile
		do
			is_destroyed := True
		end

invariant
note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
			N'zu� Kouadio Ange Kouassi & Olivier M
			D�partement des techniques de l'informatique
			C�gep de Drummondville
			960, rue Saint-Georges
			Drummondville, (Qu�bec)
			J2C 6A2
			T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859
			Site web: http://www.cegepdrummond.ca
		]"
end
