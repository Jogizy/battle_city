note
	description: "Classe de la tuile roi."
	auteur: "N'zu� Kouadio Ange Kouassi & Olivier M"
	date: "2020-04-14"
	revision: "1.0"

class
	TILE_KING

inherit
	GAME_RANDOM_SHARED
	TILE_BREAKABLE
		redefine
			make
		end

create
	make

feature {NONE} -- Initialisation

	make(a_audio_factory:AUDIO_SOURCE_FACTORY)
			-- Initialisation de `Current' � utiliser avec `a_audio_factory'
		do
			precursor(a_audio_factory)
			is_broken := False
		end

	initialize_tile_coordinate
			-- Initialisation al�atoire des coordonn�es de `Current'
		local
			l_entier:INTEGER
		do
			random.generate_new_random
			l_entier := random.last_random_integer_between(1,7)
			sub_image_x := 256 + (16 * l_entier)
			sub_image_y := 224
		end

feature -- Acc�s

	disintegrate_king
			-- Change l'aspect de `current'
		do
			sub_image_x := 320
			sub_image_y := 32
		end

	play_hit_king
			-- Joue le son du bloc {TILE_STEEL} touch�
		do
			sound_hit_king.play(0)
		end

invariant
note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
			N'zu� Kouadio Ange Kouassi & Olivier M
			D�partement des techniques de l'informatique
			C�gep de Drummondville
			960, rue Saint-Georges
			Drummondville, (Qu�bec)
			J2C 6A2
			T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859
			Site web: http://www.cegepdrummond.ca
		]"
end
