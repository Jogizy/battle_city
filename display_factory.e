note
	description: "Classe abstraite permettant l'assignation de la position des objets � afficher"
	auteur: "N'zu� Kouadio Ange Kouassi & Olivier M"
	date: "2020-04-14"
	revision: "1.0"

deferred class
	DISPLAY_FACTORY

feature {NONE} -- Impl�mentation

	set_position(a_x, a_y:INTEGER; a_display:DISPLAY)
			-- Assignation de la position des objets � afficher
		do
			a_display.set_x(a_x)
			a_display.set_y(a_y)
		end

invariant
note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
			N'zu� Kouadio Ange Kouassi & Olivier M
			D�partement des techniques de l'informatique
			C�gep de Drummondville
			960, rue Saint-Georges
			Drummondville, (Qu�bec)
			J2C 6A2
			T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859
			Site web: http://www.cegepdrummond.ca
		]"
end
