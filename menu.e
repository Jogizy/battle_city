note
	description: "Le Menu du jeu ."
	auteur: "N'zu� Kouadio Ange Kouassi & Olivier M"
	date: "2020-05-29"
	revision: "1.0"

class
	MENU
inherit
	GAME_LIBRARY_SHARED
	ENGINE_TEXT
		redefine
			make
		end

create
	make

feature {NONE} -- Initialisation

	make(a_window:GAME_WINDOW_RENDERED)
			-- Initialisation de `Current' � utiliser avec `a_window'
		do
			precursor(a_window)
			is_game := False
			initialize_font
			create tile.make(96,0)
			initialize_text
		end

	initialize_font
			-- Initialise les diff�rentes polices de texte
		do
			big_font:= load_font ("font.ttf",20)
			medium_font:= load_font ("font.ttf",15)
			small_font:= load_font ("font.ttf",9)
		end

	initialize_text
			-- Initialise les textes de `current'
		do
			texts.extend ([40, 40, "BATTLE - CITY", create {GAME_COLOR}.make_rgb(0,0,0),big_font])
			texts.extend ([70, 80, "1-START ", create {GAME_COLOR}.make_rgb(204,102,0),medium_font])
			texts.extend ([85, 95, "press 'enter'", create {GAME_COLOR}.make_rgb(224,224,224),small_font])
			texts.extend ([70, 115, "2-QUIT ", create {GAME_COLOR}.make_rgb(204,102,0),medium_font])
			texts.extend ([85, 130,"press 'esc'", create {GAME_COLOR}.make_rgb(224,224,224),small_font])
		end

feature -- Acc�s

	run
			-- Lance le jeu
		do
			tile.x := 20
			tile.y:= 40
			game_library.quit_signal_actions.extend(agent on_quit)
			window.key_pressed_actions.extend(agent on_key_pressed)
			game_library.iteration_actions.extend(agent on_iteration)
			if window.renderer.driver.is_present_synchronized_supported then
				game_library.launch_no_delay
			else
				game_library.launch
			end
			game_library.clear_all_events
		end


	on_iteration(a_timestamp:NATURAL_32)
			-- �v�nement qui est lanc� � chaque it�ration.
		do
			-- Redessine la sc�ne
			window.renderer.set_drawing_color(create {GAME_COLOR}.make_rgb (0, 204, 204)) -- Couleur de l'arri�re plan
			window.renderer.draw_filled_rectangle(0, 0, texture.width, texture.height)
				draw_tile
				draw_texts

			-- Met � jour la modification � l'�cran
			window.renderer.present
		end

	on_key_pressed(a_timestamp:NATURAL_32; a_key_event:GAME_KEY_EVENT)
			-- Action lorsqu'une touche du clavier a �t� enfonc�e
		do
			if not a_key_event.is_repeat then
				if a_key_event.is_return or a_key_event.is_1  or a_key_event.is_keypad_1 then
					game_play
				elseif a_key_event.is_escape or a_key_event.is_2  or a_key_event.is_keypad_2 then
					on_quit(a_timestamp)
				end
			end
		end

	on_quit(a_timestamp: NATURAL_32)
			-- Cette m�thode est appel�e lorsque le signal de sortie est envoy�
			-- � l'application (ex: bouton X de la fen�tre).
		do
			-- Arr�te la boucle du contr�leur (autorise game_library.launch � revenir)
			game_library.stop
		end

	draw_tile
			-- Dessine un {TILE_TANK} � l'�cran
		do
			window.renderer.draw_sub_texture(
				texture,
				tile.sub_image_x, tile.sub_image_y,
				tile.sub_image_width, tile.sub_image_height,
				tile.x, tile.y
				)
		end

	big_font,small_font,medium_font:TEXT_FONT
			-- Les diff�rentes tailles de la police de texte de `current'

	tile:TILE_TANK
			-- La tuile{TILE_TANK} � afficher dans `current'

	is_game:BOOLEAN
			-- Vrai si c'est le jeu, sinon faux

	game_play
			-- Cette m�thode permet de sortir de `current' et d'acc�der au jeu
		do
			is_game := True
			game_library.stop
		end


invariant
note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
			N'zu� Kouadio Ange Kouassi & Olivier M
			D�partement des techniques de l'informatique
			C�gep de Drummondville
			960, rue Saint-Georges
			Drummondville, (Qu�bec)
			J2C 6A2
			T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859
			Site web: http://www.cegepdrummond.ca
		]"
end
