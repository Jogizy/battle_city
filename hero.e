note
	description: "L'unit� de type HERO (le personnage principal du jeu)"
	auteur: "N'zu� Kouadio Ange Kouassi & Olivier M"
	date: "2020-04-14"
	revision: "2.0"

class
	HERO

inherit
	COLLISABLE
		rename
			width as sub_image_width,
			height as sub_image_height
		end
	UNIT

create
	make

feature {NONE} -- Initialisation

	initialize_animation_coordinate
			-- Initialisation des coordonn�es d'animation de `Current'
		do
			animation_coordinates_vertical.extend([0,0])
			animation_coordinates_vertical.extend([16,0])

			animation_coordinates_horizontal.extend([96,0])
			animation_coordinates_horizontal.extend([112,0])
		end

	initialize_speed
			-- Initialisation de la vitesse de d�placement de `Current'
		do
			movement_delta := 20
			animation_delta := 100
		end

	initialize_hitpoints
			-- Initialisation du nombre de points de vies de `Current'
		do
			hitpoints := 3
		end

feature -- Acc�s

	life_restoration
			-- Restaure la vie de `current'
		do
			initialize_hitpoints
		end

	reset_moving
			-- R�initialise le d�placement	
		do
			going_up := False
			going_down := False
			going_left := False
			going_right := False
			facing_left := False
			facing_right := True
			facing_down := False
			facing_up := False
		end


note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
			N'zu� Kouadio Ange Kouassi & Olivier M
			D�partement des techniques de l'informatique
			C�gep de Drummondville
			960, rue Saint-Georges
			Drummondville, (Qu�bec)
			J2C 6A2
			T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859
			Site web: http://www.cegepdrummond.ca
		]"
end
