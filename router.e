note
	description: "Le routeur du jeu."
	auteur: "N'zu� Kouadio Ange Kouassi & Olivier M"
	date: "2020-04-14"
	revision: "1.0"

class
	ROUTER

create
	make

feature {NONE} -- Initialization
	make(a_socket:detachable NETWORK_STREAM_SOCKET)
			-- Initialisation de `Current' � utiliser avec `a_socket'
		local
			l_window_builder:GAME_WINDOW_RENDERED_BUILDER
		do
			has_error := False
			socket := a_socket
			create l_window_builder
			l_window_builder.set_dimension(208, 208)
			l_window_builder.enable_resizable
			l_window_builder.set_title("battle-city")
			l_window_builder.enable_must_renderer_synchronize_update
			window := l_window_builder.generate_window
			window.renderer.set_logical_size(208, 208)
			load_icon
		end

	load_icon
			-- Chargement de l'icone de la fen�tre du jeu
		local
			l_icon:GAME_SURFACE
			l_icon_image:GAME_IMAGE_BMP_FILE
		do
			create l_icon_image.make("icon.bmp")
			if l_icon_image.is_openable then
				l_icon_image.open
				if l_icon_image.is_open then
					create l_icon.share_from_image(l_icon_image)
					l_icon.set_transparent_color
						(create {GAME_COLOR}.make_rgb(255, 0, 255))
					window.set_icon(l_icon)
				else
					has_error := True
				end
			else
				has_error := True
			end
		end

feature -- Acc�s

	is_quit:BOOLEAN
			-- vrai si quitt�, sinon faux

	is_menu:BOOLEAN
			-- Vrai si c'est le menu, sinon faux

	is_game:BOOLEAN
			-- Vrai si c'est le jeu, sinon faux

	has_error:BOOLEAN
			-- "True" si une erreur s'est produite lors de la cr�ation de `Current'

	window:GAME_WINDOW_RENDERED
			-- La fen�tre pour dessiner la sc�ne

	socket:detachable NETWORK_STREAM_SOCKET
			-- Le socket de communication de `Current'

	run
			-- Lance en premier le {MENU} puis le {GAME_NETWORK_ENGINE}
		do
			is_menu := True
			from
				is_quit := False
			until
				is_quit
			loop
				if is_menu then
					run_menu
				elseif is_game then
					run_game_engine
				end
			end
		end

	run_viewer
			-- Lance la visionneuse de jeu
		local
			l_viewer:VIEWER_ENGINE
		do
			window.set_title ("viewer")
			if attached socket as la_socket then
				create l_viewer.make (la_socket, window)
				if not l_viewer.has_error then
					l_viewer.run
				end
			end
		end

	run_menu
			-- Ex�cute le {MENU} dans `window 'et g�re sa sortie.
		local
			l_menu:MENU
		do
			create l_menu.make (window)
			if not l_menu.has_error then
				l_menu.run
				is_game := l_menu.is_game
				is_quit := not is_game
				is_menu:= False
			end

		end

	run_game_engine
			-- Ex�cute le {GAME_NETWORK_ENGINE} dans `window 'et g�re sa sortie
		local
			l_game_engine:GAME_NETWORK_ENGINE
		do
			create l_game_engine.make (socket, window)
			if not l_game_engine.has_error then
				run_steps(l_game_engine)
				if not is_quit then
					is_menu := l_game_engine.is_menu or l_game_engine.game_over  or l_game_engine.you_win
					is_quit := not is_menu
				end
				is_game:= False
			end
		end

	run_steps(a_game_engine:GAME_NETWORK_ENGINE)
			-- Ex�cute les diff�rentes �tapes qui se produisent lors du jeu
		local
			l_is_new_level:BOOLEAN
			l_level:INTEGER
		do
			l_level :=1
			from
				l_is_new_level:= True
			until
				not l_is_new_level
			loop
				run_text_poster(30,"STAGE" + " " + l_level.out,47,79,79)
				if is_quit then
					l_is_new_level := False
				else
					a_game_engine.set_level(l_level)
					a_game_engine.run
					l_is_new_level:= a_game_engine.is_next_level
					l_level := l_level + 1
					if  a_game_engine.game_over then
						run_text_poster(22,"GAME OVER",255,0,0)
					elseif a_game_engine.you_win then
						run_text_poster(25,"YOU WIN !",0,102,0)
					end
				end
			end
		end

	run_text_poster(a_font_size:INTEGER;a_text:STRING; a_red_code,a_green_code,a_blue_code:NATURAL_8)
			-- Lance le {TEXT_POSTER} pour afficher le type de message en utilisant `a_font_size',`a_text',
			-- `a_red_code',`a_green_code' et `a_blue_code'
		local
			l_text_poster:TEXT_POSTER
		do
			create l_text_poster.make (window,a_font_size, a_text,a_red_code,a_green_code,a_blue_code)
			if not l_text_poster.has_error then
				l_text_poster.run
				is_quit := l_text_poster.is_quit
			end
		end

invariant
note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
			N'zu� Kouadio Ange Kouassi & Olivier M
			D�partement des techniques de l'informatique
			C�gep de Drummondville
			960, rue Saint-Georges
			Drummondville, (Qu�bec)
			J2C 6A2
			T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859
			Site web: http://www.cegepdrummond.ca
		]"
end
