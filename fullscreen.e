note
	description: "Cette classe change le mode d'affichage de la fen�tre"
	auteur: "Olivier Meloche & N'zu� Kouadio Ange Kouassi"
	date: "2020-02-17"
	revision: "1.0"

class
	FULLSCREEN

create
	make

feature {NONE} -- Initialisation

	make(a_window:GAME_WINDOW)
			-- Initialisation de `Current ' � utiliser avec `a_window'
		do
			window := a_window
		end

feature -- Acc�s

	toggle_fullscreen
			-- Inverse le pr�sent mode d'affichage soit plein �cran ou en mode fen�tre
		do
			if window.is_fullscreen then
				window.set_windowed
			else
				window.set_fake_fullscreen
			end
		end

	window:GAME_WINDOW
			-- Le mode d'affichage en cours

invariant
note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
			N'zu� Kouadio Ange Kouassi & Olivier M
			D�partement des techniques de l'informatique
			C�gep de Drummondville
			960, rue Saint-Georges
			Drummondville, (Qu�bec)
			J2C 6A2
			T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859
			Site web: http://www.cegepdrummond.ca
		]"
end
