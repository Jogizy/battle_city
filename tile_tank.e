note
	description: "Classe de la tuile tank."
	auteur: "N'zu� Kouadio Ange Kouassi & Olivier M"
	date: "2020-04-14"
	revision: "1.0"

class
	TILE_TANK

inherit
	TILE
		rename
			make as make_tile
		end
create
	make

feature {NONE} -- Initialisation
	make(a_x,a_y:INTEGER)
			-- Initialisation de `Current' � utiliser avec `a_x' et `a_y'
		do
			make_tile
			x_coordinate := a_x
			y_coordinate := a_y
			initialize_tile_coordinate
		end

	initialize_tile_coordinate
			-- Initialisation des coordonn�es de `Current'
		do
			sub_image_x := x_coordinate
			sub_image_y := y_coordinate
		end

feature -- Acc�s

	x_coordinate, y_coordinate:INTEGER
		-- Les coordonn�es de la partie de l'image � afficher � l'int�rieur de la `surface'

invariant
note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
			N'zu� Kouadio Ange Kouassi & Olivier M
			D�partement des techniques de l'informatique
			C�gep de Drummondville
			960, rue Saint-Georges
			Drummondville, (Qu�bec)
			J2C 6A2
			T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859
			Site web: http://www.cegepdrummond.ca
		]"
end
