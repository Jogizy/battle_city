note
	description: "Classe de la tuile acier."
	auteur: "N'zu� Kouadio Ange Kouassi & Olivier M"
	date: "2020-04-14"
	revision: "1.0"

class
	TILE_STEEL

inherit
	TILE_COLLISABLE

create
	make

feature {NONE} -- Initialisation

	initialize_tile_coordinate
			-- Initialisation des coordonn�es de `Current'
		do
			sub_image_x := 256
			sub_image_y := 16
		end

feature -- Acc�s

	play_hit_steel
			-- Joue le son du bloc d'acier touch�
		do
			sound_hit_steel.play(0)
		end

invariant
note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
			N'zu� Kouadio Ange Kouassi & Olivier M
			D�partement des techniques de l'informatique
			C�gep de Drummondville
			960, rue Saint-Georges
			Drummondville, (Qu�bec)
			J2C 6A2
			T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859
			Site web: http://www.cegepdrummond.ca
		]"
end
