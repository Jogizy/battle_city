note
	description: "Classe r�seau pour le client du jeu."
	auteur: "N'zu� Kouadio Ange Kouassi & Olivier M"
	date: "2020-05-04"
	revision: "1.0"

class
	NETWORK_CUSTOMER_GAME

inherit
	THREAD
		rename
			make as make_thread
		end

create
	make

feature {NONE} -- Initialisation

	make(a_socket:NETWORK_STREAM_SOCKET)
			-- Initialisation de `Current' � utiliser avec `a_socket'
		do
			make_thread
			socket := a_socket
		end

	socket:NETWORK_STREAM_SOCKET
			-- Le socket de communication de `Current'

feature -- Acc�s
	stop
			-- Terminer `Current'
		do
			must_stop := True
		end

	execute
			-- L'ex�cution du {THREAD} de `Current'
		local
			l_retreived:detachable ANY
		do
			from must_stop := False
			until must_stop
			loop
				l_retreived := socket.retrieved
				if attached {LIST[DISPLAY]}l_retreived as la_displays then
					displays :=	la_displays
				else
					must_stop := True
				end
			end
		end

	send
			-- Envoie le signal du socket � `True'
		do
			socket.put_boolean(True)
		end

	must_stop:BOOLEAN
			-- Vrai si la proc�dure de tir n'est pas ex�cut�

	displays:detachable LIST[DISPLAY]
			-- La liste d'objets affichables

invariant
note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
			N'zu� Kouadio Ange Kouassi & Olivier M
			D�partement des techniques de l'informatique
			C�gep de Drummondville
			960, rue Saint-Georges
			Drummondville, (Qu�bec)
			J2C 6A2
			T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859
			Site web: http://www.cegepdrummond.ca
		]"
end
