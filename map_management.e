note
	description: "Classe permettant la gestion des cartes du jeu"
	auteur: "N'zu� Kouadio Ange Kouassi & Olivier M"
	date: "2020-04-20"
	revision: "2.1"

class
	MAP_MANAGEMENT

create
	make

feature {NONE} -- Initialisation

	make(a_tile_factory:TILE_FACTORY)
			-- Initialisation de `Current' � utiliser avec `a_tile_factory'
		do
			create {ARRAYED_LIST[MAP]}maps.make(3)
			maps.extend(create {MAP_1}.make(a_tile_factory))
			maps.extend(create {MAP_2}.make(a_tile_factory))
			maps.extend(create {MAP_3}.make(a_tile_factory))
			current_map := maps.first
		ensure
			valid_map: maps.count > 0
		end

feature -- Acc�s

	current_map:MAP
			-- La carte en cours

	maps:LIST[MAP]
			-- La liste des cartes

	change_map(a_number:INTEGER)
			-- Change `current_map' en fonction de l'index entier `a_number' de la liste `maps'
		require
			valid_number:a_number >= 1 and a_number <= maps.count
		do
			current_map := maps[a_number]
		ensure
			map_change:current_map = maps[a_number]
		end

invariant
 	valid_map: maps.count > 0

note
	copyright: "Copyright (C) 2020 N'zu� Kouadio Ange Kouassi & Olivier M"
	license: "GPL 3.0(voir http:/www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
			N'zu� Kouadio Ange Kouassi & Olivier M
			D�partement des techniques de l'informatique
			C�gep de Drummondville
			960, rue Saint-Georges
			Drummondville, (Qu�bec)
			J2C 6A2
			T�l�phone: 1-819-478-4671 Fax: 1-819-474-6859
			Site web: http://www.cegepdrummond.ca
		]"
end
